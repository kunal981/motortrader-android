package com.poliveira.apps.materialtests.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.poliveira.apps.materialtests.AddyourCarFragment.AddCarDetailsFragment;
import com.poliveira.apps.materialtests.Modal.CarListModal;
import com.poliveira.apps.materialtests.NavigationDrawerFragment;
import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by ShalviSharma on 7/28/15.
 */
public class HomeFragment extends Fragment {
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    ListView listView;
    AdapterList adapter1;
    Fragment mFragment;
    TextView headerTitle;
    NavigationDrawerFragment mNavigationDrawerFragment;
    String seller_num,make,model,city,fuel_type,price,image,year,id;
    CarListModal carListModal;
    List<CarListModal> carListModalList;
    JSONObject jsonObject;
    ViewHolder holder;
    private boolean mSearchCheck;
    DisplayImageOptions options;
    View rootview;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    String newImage,getImage;
    String back ="0";
    AppCompatActivity activity;




    public static HomeFragment newInstance(String text) {
        HomeFragment mFragment = new HomeFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

         activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");
        headerTitle = (TextView) activity.findViewById(R.id.mytext);

        headerTitle.setText("Car List");

            rootview = inflater.inflate(R.layout.fragment_home, null);

         /*rootview = inflater.inflate(R.layout.fragment_home, container, false);*/
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
/**
 * check internet connection
 */
        checkInternet();
        listView = (ListView) rootview.findViewById(R.id.listview_home);




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Toast.makeText(getActivity(),"Hello" +position,Toast.LENGTH_SHORT).show();

                final CarListModal carListModal = carListModalList.get(position);
                String uid = carListModal.getId();


                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                CarDetailsFragment fragment = new CarDetailsFragment();
                Bundle savedInstanceState = new Bundle();
                savedInstanceState.putString("UUID_PASSED", uid);

                fragmentTransaction.replace(R.id.container, fragment).addToBackStack(null);
                fragment.setArguments(savedInstanceState);
                fragmentTransaction.commit();

            }
        });


        rootview.setFocusableInTouchMode(true);
        rootview.requestFocus();
        rootview.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub


                if (keyCode == KeyEvent.KEYCODE_BACK && back.equals("0")) {
                    back = "1";

                    Toast.makeText(getActivity(), "Press again to exit the app!",Toast.LENGTH_SHORT).show();
                    return true;

                } else {
                    return false;
                }

            }
        });


        /**
         * API execution
         */
        new ExecuteList().execute();


        return rootview;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);


        final MenuItem menuItem = menu.findItem(R.id.menu_search);
        menuItem.setVisible(false);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint(this.getString(R.string.search));

        ((EditText) searchView.findViewById(R.id.search_src_text))
                .setHintTextColor(Color.WHITE);




        //menu.findItem(R.id.menu_add).setVisible(true);

        mSearchCheck = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        FragmentManager mFragmentManager = getActivity().getSupportFragmentManager();
        switch (item.getItemId()) {

            case R.id.menu_add:
                //Toast.makeText(getActivity(), R.string.add, Toast.LENGTH_SHORT).show();


                FragmentManager fragmentManager = getFragmentManager();

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                AddCarDetailsFragment fragment = new AddCarDetailsFragment();
                fragmentTransaction.replace(R.id.container, fragment).addToBackStack(fragment.getClass().getName());

                fragmentTransaction.commit();


                break;

            case R.id.menu_search:
                mSearchCheck = true;

                //Toast.makeText(getActivity(), R.string.search, Toast.LENGTH_SHORT).show();

                break;
        }
        return true;
    }
    private DisplayImageOptions setupImageLoader() {
        return new DisplayImageOptions.Builder().cacheInMemory(true)
                .showImageOnLoading(R.drawable.img_loading)
                .showImageForEmptyUri(R.drawable.no_image_icon)
                .showImageOnFail(R.drawable.no_image_icon)
                .cacheOnDisk(true).considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(1)).build();

    }


    class AdapterList extends BaseAdapter {
        List<CarListModal> carListModalList=null;
        DisplayImageOptions options;
        private Context context;
        private LayoutInflater inflater;
        private ArrayList<CarListModal> arraylist;


        public AdapterList(Context context,List<CarListModal> carListModalList) {

            this.context = context;
            this.carListModalList = carListModalList;
            this.arraylist = new ArrayList<CarListModal>();
            this.arraylist.addAll(carListModalList);
            options = setupImageLoader();

        }

        @Override
        public int getCount() {
            return carListModalList.size();
        }

        @Override
        public Object getItem(int position) {
            return carListModalList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {


            if (inflater == null) {
                inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            }


            if (convertView == null) {
                convertView = inflater.inflate(R.layout.home_items_, null);
                holder = new ViewHolder();

                holder.imageView = (ImageView)convertView.findViewById(R.id.img_event_id_car);
                holder.txtHeading = (TextView)convertView.findViewById(R.id.txt1);
                holder.txtPrice = (TextView)convertView.findViewById(R.id.txt_car_price);
                holder.txtNumber = (TextView)convertView.findViewById(R.id.txt_number);
                holder.txtFuel = (TextView)convertView.findViewById(R.id.txt_oil);
                holder.txtlocation = (TextView)convertView.findViewById(R.id.txt_city);
                holder.txtyear = (TextView)convertView.findViewById(R.id.txt_year);
                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            final CarListModal carListModal = carListModalList.get(position);
            holder.txtHeading.setText(carListModal.getMake()+" "+ carListModal.getModel());
            holder.txtPrice.setText(carListModal.getPrice());
            holder.txtNumber.setText(carListModal.getSeller_num());
            holder.txtFuel.setText(carListModal.getFuel_type());
            holder.txtlocation.setText(carListModal.getCity());
            holder.txtyear.setText(carListModal.getYear());

             getImage = carListModal.getImage();

            if (getImage!=null){
                ImageLoader.getInstance().displayImage(getImage, holder.imageView, options);
            }

            /*holder.txtPrice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   *//* FragmentManager fragmentManager = getFragmentManager();

                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    PaymentFragment fragment = new PaymentFragment();
                    fragmentTransaction.replace(R.id.container, fragment);

                    fragmentTransaction.commit();*//*


                    Intent i  = new Intent(getActivity(), PaymentActivity.class);
                    i.putExtra("Price",carListModal.getPrice());
                    startActivity(i);
                }
            });
*/



            return convertView;
        }


        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            carListModalList.clear();
            if (charText.length() == 0) {
                carListModalList.addAll(arraylist);
            } else {
                for (CarListModal wp : arraylist) {
                    if (wp.getModel().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        carListModalList.add(wp);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }
        class ExecuteList extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            String url = WSConnector.getCarList();
            return url;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
           UI.hideProgressDialog();
            if (result.contains("true")){
                carListModalList=updateUI(result);
                adapter1 = new AdapterList(getActivity(),carListModalList);
                listView.setAdapter(adapter1);
            }
        }
    }

    public List<CarListModal> updateUI(String result) {

        List<CarListModal> carListModalList =  new ArrayList<CarListModal>();

        try {

            jsonObject = new JSONObject(result);

            JSONArray jsonArray = jsonObject.getJSONArray("data");

            for (int i=0;i<=jsonArray.length();i++){
                CarListModal carListModal = new CarListModal();

                JSONObject jsonInnerObject = jsonArray.getJSONObject(i);

                id =  jsonInnerObject.getString("id");
                seller_num = jsonInnerObject.getString("seller_num");
                make = jsonInnerObject.getString("make");
                model = jsonInnerObject.getString("model");
                city = jsonInnerObject.getString("city");
                fuel_type = jsonInnerObject.getString("fuel_type");
                price = jsonInnerObject.getString("price");
                image = jsonInnerObject.getString("image");
                year = jsonInnerObject.getString("year");

//                newImage = image.replaceAll("[^a-zA-Z0-9/]", "-");




                carListModal.setId(id);
                carListModal.setSeller_num(seller_num);
                carListModal.setMake(make);
                carListModal.setModel(model);
                carListModal.setCity(city);
                carListModal.setFuel_type(fuel_type);
                carListModal.setPrice(price);
                carListModal.setImage(image);
                carListModal.setYear(year);



                carListModalList.add(carListModal);


            }


        }catch (Exception e){
            e.printStackTrace();
        }


        return carListModalList;
    }

    static class ViewHolder {
        ImageView imageView;
        TextView txtHeading, txtPrice, txtNumber, txtFuel, txtlocation,
                txtyear;

    }



    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }



}
