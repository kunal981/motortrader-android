package com.poliveira.apps.materialtests.SpareParts;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONObject;


/**
 * Created by ${ShalviSharma} on 7/29/15.
 */
public class SpareWritetoUsFragment extends Fragment {
    TextView headerTitle;
    Button btnSend;
    EditText edtname,edtemail,edtphone,edtmessage;
    String strname,stremail,strPhone,strMesage,spare_id;
    ConnectionDetector cd;
    boolean isInternetPresent = false;

    JSONObject jsonObject;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View rootview = inflater.inflate(R.layout.fragment_spare_write,container,false);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
        headerTitle = (TextView) activity.findViewById(R.id.mytext);
        edtname = (EditText)rootview.findViewById(R.id.edt_name_spare);
        edtemail = (EditText)rootview.findViewById(R.id.edt_email_spare);
        edtphone = (EditText)rootview.findViewById(R.id.edt_phone_spare);
        edtmessage = (EditText)rootview.findViewById(R.id.edt_message_spare);
        btnSend = (Button)rootview.findViewById(R.id.button_send_spare);

        headerTitle.setText("Write to us");
        checkInternet();

        if (getArguments()!=null){
            Bundle bundle = this.getArguments();
            spare_id = bundle.getString("SPARE_ID");
        }



        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strname = edtname.getText().toString();
                stremail = edtemail.getText().toString();
                strPhone = edtphone.getText().toString();
                strMesage = edtmessage.getText().toString();

                boolean receive = validatation_method();
                if (receive && isInternetPresent) {
                    new ExecuteEnquiry().execute(spare_id,strname,stremail,strPhone,strMesage);
                }




            }
        });
        return rootview;

    }


    private boolean validatation_method() {
        // TODO Auto-generated method stub
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";
        if (edtname.getText().toString().equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your name", "OK");
            return false;
        }
        if (edtemail.getText().toString().equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your email address", "OK");
            return false;
        }

        if (!edtemail.getText().toString().matches(emailPattern)) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter valid email address", "OK");
            return false;
        }

        if (edtphone.getText().toString().matches("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter phone number", "OK");
            return false;
        }



        if (edtmessage.getText().toString().equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your message", "OK");
            return false;
        }





        return true;
    }




    class ExecuteEnquiry extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            String url = WSConnector.postWritetoUsSpare(params[0],params[1],params[2],params[3],params[4]);


            return url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
           UI.hideProgressDialog();

            if (result.contains("true")){
                updateUI(result);
            }

        }
    }
    public void updateUI(String result) {

        try {

            jsonObject = new JSONObject(result);

            String message = jsonObject.getString("message");

            if (message!=null){

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("").setMessage(message).setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                                getFragmentManager().popBackStackImmediate();
                            }
                        });

                AlertDialog dialog = alertDialog.create();
                dialog.show();
                TextView messageText = (TextView) dialog
                        .findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER);

            }





        }catch (Exception e){

        }

    }



    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet

            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");
        }
    }


}
