package com.poliveira.apps.materialtests.SettingsFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SwitchCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.poliveira.apps.materialtests.R;

/**
 * Created by ${ShalviSharma} on 7/30/15.
 */
public class SettingFragment extends Fragment {

    RelativeLayout relativeLayoutChange,relativePrivacy,relative_about;
    SwitchCompat switchCompat;
    String back ="0";
    public static SettingFragment newInstance() {
        SettingFragment mFragment = new SettingFragment();

        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_setting,container,false);

        relativeLayoutChange = (RelativeLayout)rootview.findViewById(R.id.relative_change);
        relativePrivacy = (RelativeLayout)rootview.findViewById(R.id.relative_privacy);
        relative_about = (RelativeLayout)rootview.findViewById(R.id.relative_about);

        switchCompat = (SwitchCompat)rootview.findViewById(R.id.switch_alert);
        relativeLayoutChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ChangePasswordFragment fragment = new ChangePasswordFragment();
                fragmentTransaction.replace(R.id.container, fragment).addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(getActivity(), "Alerts are on!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Alerts are off!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        relativePrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                PrivacyPolicyFragment fragment = new PrivacyPolicyFragment();
                fragmentTransaction.replace(R.id.container, fragment).addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        relative_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                AboutUsFragment fragment = new AboutUsFragment();
                fragmentTransaction.replace(R.id.container, fragment).addToBackStack(null);
                fragmentTransaction.commit();
            }
        });



        rootview.setFocusableInTouchMode(true);
        rootview.requestFocus();
        rootview.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub


                if (keyCode == KeyEvent.KEYCODE_BACK && back.equals("0")) {
                    back = "1";

                    Toast.makeText(getActivity(), "Press again to exit the app!", Toast.LENGTH_SHORT).show();
                    return true;

                } else {
                    return false;
                }

            }
        });
        return rootview;
    }
}
