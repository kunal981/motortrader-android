package com.poliveira.apps.materialtests.PaymentGateway;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.poliveira.apps.materialtests.R;

import java.net.URLEncoder;

/**
 * Created by ${ShalviSharma} on 9/21/15.
 */
public class PaymentActivity extends Activity {
    private WebView webView;
    private ProgressBar progress;
    String customerId,email,number;
    boolean test=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        webView = (WebView) findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebViewClient(new MyWebViewClient());



        progress = (ProgressBar) findViewById(R.id.progressBar);
        progress.setVisibility(View.GONE);

        email = getIntent().getStringExtra("emailAddress");
        number = getIntent().getStringExtra("number");
        customerId = getIntent().getStringExtra("customerId");



        StringBuffer buffer=new StringBuffer("https://www.motortrader.ng/quick_teller_payment/");
        buffer.append("?paymentCode=" + URLEncoder.encode("99401"));
        buffer.append("&amount=" + URLEncoder.encode("50000"));
        buffer.append("&customerId=" + URLEncoder.encode(customerId));
        buffer.append("&mobileNumber="+ URLEncoder.encode(number));
        buffer.append("&emailAddress=" + URLEncoder.encode(email));

        webView.loadUrl(buffer.toString());

        /*price = getIntent().getStringExtra("price");
        email = getIntent().getStringExtra("email");
        number = getIntent().getStringExtra("number");*/
        //Log.e("price", "" + price);
      /*  webView.loadUrl("https://www.quickteller.com/motortrader");*/

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            Log.e("URL", "" + url);




            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progress.setVisibility(View.GONE);
            PaymentActivity.this.progress.setProgress(100);
            Log.e("onPageFinished", "" + view.getOriginalUrl());







            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progress.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
            PaymentActivity.this.progress.setProgress(0);


            //view.loadUrl("javascript:document.getElementById('amount').value = '" + 300.00 + "';");
            // view.loadUrl("javascript:document.getElementById('amount').value = '"+300.00+"';");
            Log.e("Starturll", "" + view.getUrl());
        }


        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);

            handler.proceed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.web_view, menu);
        return true;
    }

    public void setValue(int progress) {
        this.progress.setProgress(progress);
    }
}

