package com.poliveira.apps.materialtests.SearchCars;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.poliveira.apps.materialtests.Modal.CarListModal;
import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ${ShalviSharma} on 7/29/15.
 */
public class SearchCarsFragment extends Fragment {
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    private boolean mSearchCheck;
    String makeid, modelId, yearSearch, location, bodytypel, transType, fuelType;
    String seller_num,make,model,city,fuel_type,price,image,year,id,desc;
    ProgressDialog pDialog;
    ViewHolder holder;
    ListView listView_search;
    LAdapter adapter_search;
    List<CarListModal> carListModalList;
    JSONObject jsonObject;
    String message;
    JSONArray jsonArray;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    String back ="0";
    AppCompatActivity activity;
    TextView headerTitle;
    String newImage,getImage;
    public static SearchCarsFragment newInstance(String text) {
        SearchCarsFragment mFragment = new SearchCarsFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_search_cars, container, false);
        listView_search = (ListView) rootview.findViewById(R.id.listview_search);
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
/**
 * check internet connection
 */
        checkInternet();

        activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");
        headerTitle = (TextView) activity.findViewById(R.id.mytext);

        headerTitle.setText("Search Car");

        if (getArguments() != null) {
            Bundle bundle = this.getArguments();
            makeid = bundle.getString("MAKE_ID");
            modelId = bundle.getString("MODEL_ID");
            yearSearch = bundle.getString("SELECTED_YEAR");
            location = bundle.getString("LOCATION");
            bodytypel = bundle.getString("BODY_TYPE");
            fuelType = bundle.getString("FUEL_TYPE");
            transType = bundle.getString("TRANS_TYPE");
        }


        /**
         * API execution
         */
        new ExecuteSearch().execute(makeid, modelId, yearSearch, location, fuelType, transType, bodytypel);

        listView_search.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final  CarListModal  carListModal = carListModalList.get(position);
                String ID = carListModal.getId();
                Log.e("ID",""+ID);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SearchCarDetailsFragment fragment = new SearchCarDetailsFragment();
                Bundle savedInstanceState = new Bundle();
                savedInstanceState.putString("ID", ID);
                fragmentTransaction.replace(R.id.container, fragment).addToBackStack(null);
                fragment.setArguments(savedInstanceState);
                fragmentTransaction.commit();
            }
        });

        rootview.setFocusableInTouchMode(true);
        rootview.requestFocus();
        rootview.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub


                if (keyCode == KeyEvent.KEYCODE_BACK && back.equals("0")) {
                    back = "1";

                    Toast.makeText(getActivity(), "Press again to exit the app!", Toast.LENGTH_SHORT).show();
                    return true;

                } else {
                    return false;
                }

            }
        });


        return rootview;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);


        menu.findItem(R.id.menu_search).setVisible(false);

        menu.findItem(R.id.menu_add).setVisible(false);


    }

  /*  @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        FragmentManager mFragmentManager = getActivity().getSupportFragmentManager();
        switch (item.getItemId()) {

            case R.id.menu_add:
                Toast.makeText(getActivity(), R.string.add, Toast.LENGTH_SHORT).show();
                break;


        }
        return true;
    }*/


    class LAdapter extends BaseAdapter {
        private Context context;
        private LayoutInflater inflater;
        List<CarListModal> carListModalList;
        DisplayImageOptions options;

        public LAdapter(Context context, List<CarListModal> carListModalList) {
            this.context = context;
            this.carListModalList = carListModalList;
            options = setupImageLoader();
        }

        @Override
        public int getCount() {
            return carListModalList.size();
        }

        @Override
        public Object getItem(int position) {
            return carListModalList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {


            if (inflater == null) {
                inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            }


            if (convertView == null) {
                convertView = inflater.inflate(R.layout.search_items_, null);
                holder = new ViewHolder();

                holder.imageView = (ImageView)convertView.findViewById(R.id.img_search_car);
                holder.txtHeading = (TextView)convertView.findViewById(R.id.txt_search_heading);
                holder.txtPrice = (TextView)convertView.findViewById(R.id.txt_search_car_price);
                holder.txtNumber = (TextView)convertView.findViewById(R.id.txt_search_number);
                holder.txtFuel = (TextView)convertView.findViewById(R.id.txtsearch_oil);
                holder.txtlocation = (TextView)convertView.findViewById(R.id.txt_search_city);
                holder.txtyear = (TextView)convertView.findViewById(R.id.txt_search_year);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final CarListModal carListModal = carListModalList.get(position);

            holder.txtHeading.setText(carListModal.getModel());
            holder.txtPrice.setText(carListModal.getPrice());
            holder.txtNumber.setText(carListModal.getSeller_num());
            holder.txtFuel.setText(carListModal.getFuel_type());
            holder.txtlocation.setText(carListModal.getCity());
            holder.txtyear.setText(carListModal.getYear());

            getImage = carListModal.getImage();

            if (getImage!=null){
                ImageLoader.getInstance().displayImage(getImage, holder.imageView, options);
            }
            return convertView;
        }
    }



    class ExecuteSearch extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            String url = WSConnector.postAdvanceSearch(params[0],params[1],params[2],params[3],params[4],params[5],params[6]);
            return url;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
         UI.hideProgressDialog();
            if (result.contains("true")){

                Log.e("result--",""+result);
                carListModalList=updateUI(result);
                if (carListModalList!=null){
                    adapter_search = new LAdapter(getActivity(),carListModalList);
                    listView_search.setAdapter(adapter_search);
                }


            }
            else if (result.contains("false")){

                /*Toast.makeText(getActivity(),"No records found",Toast.LENGTH_SHORT).show();*/
                LogMessage.showDialog(getActivity(),"","No records found","OK");

            }
        }



    }







    public List<CarListModal> updateUI(String result) {

        List<CarListModal> carListModalList =  new ArrayList<CarListModal>();

        try {

            jsonObject = new JSONObject(result);

             jsonArray = jsonObject.getJSONArray("data");
           /* message = jsonArray.getString("message");*/



            for (int i=0;i<=jsonArray.length();i++){
                CarListModal carListModal = new CarListModal();

                JSONObject jsonInnerObject = jsonArray.getJSONObject(i);

                id= jsonInnerObject.getString("id");
                seller_num = jsonInnerObject.getString("seller_num");
                make = jsonInnerObject.getString("make");
                model = jsonInnerObject.getString("model");
                city = jsonInnerObject.getString("location");
                fuel_type = jsonInnerObject.getString("fuel_type");
                price = jsonInnerObject.getString("price");
                image = jsonInnerObject.getString("image");
                year = jsonInnerObject.getString("year");
                desc= jsonInnerObject.getString("desc");


                carListModal.setId(id);
                carListModal.setSeller_num(seller_num);
                carListModal.setMake(make);
                carListModal.setModel(model);
                carListModal.setCity(city);
                carListModal.setFuel_type(fuel_type);
                carListModal.setPrice(price);
                carListModal.setImage(image);
                carListModal.setYear(year);
                carListModal.setDesc(desc);



                carListModalList.add(carListModal);


            }


        }catch (Exception e){
            e.printStackTrace();
        }


        return carListModalList;
    }













    static class ViewHolder {
        ImageView imageView;
        TextView txtHeading, txtPrice, txtNumber, txtFuel, txtlocation,
                txtyear;

    }



    private DisplayImageOptions setupImageLoader() {
        return new DisplayImageOptions.Builder().cacheInMemory(true)
                .showImageOnLoading(R.drawable.img_loading)
                .showImageForEmptyUri(R.drawable.no_image_icon)
                .showImageOnFail(R.drawable.no_image_icon)
                .cacheOnDisk(true).considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(1)).build();

    }



    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }
}
