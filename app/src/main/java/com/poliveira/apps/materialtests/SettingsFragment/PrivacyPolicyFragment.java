package com.poliveira.apps.materialtests.SettingsFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.poliveira.apps.materialtests.R;

/**
 * Created by ${ShalviSharma} on 8/19/15.
 */
public class PrivacyPolicyFragment extends Fragment {
    TextView headerTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_privacy,container,false);

        AppCompatActivity activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");
        headerTitle = (TextView) activity.findViewById(R.id.mytext);
        headerTitle.setText("Privacy Policy");

        return rootview;
    }
}
