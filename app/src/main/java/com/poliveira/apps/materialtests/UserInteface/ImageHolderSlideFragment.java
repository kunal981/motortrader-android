package com.poliveira.apps.materialtests.UserInteface;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.poliveira.apps.materialtests.R;

/**
 * Created by ${ShalviSharma} on 8/27/15.
 */
public class ImageHolderSlideFragment extends Fragment {

    public static String TAG = "ImageHolderSlideFragment";
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";
    public static final String ARG_URL = "url";

    /**
     * The fragment's page number, which is set to the argument value for
     * {@link #ARG_PAGE}.
     */
    private int mPageNumber;
    private String imageUrl;
    static DisplayImageOptions options;
    Uri uri;
    private String mimetype;
    /**
     * Factory method for this fragment class. Constructs a new fragment for the
     * given page number.
     */
    public static ImageHolderSlideFragment create(int pageNumber, String url,
                                                  DisplayImageOptions options2) {
        ImageHolderSlideFragment fragment = new ImageHolderSlideFragment();
        options = options2;
        Bundle args = new Bundle();

        args.putInt(ARG_PAGE, pageNumber);
        args.putString(ARG_URL, url);

        fragment.setArguments(args);
        return fragment;
    }

    public ImageHolderSlideFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
        imageUrl = getArguments().getString(ARG_URL);

    }

	/*
	 *
	 * id_name id_week rating_bar id_text_desc
	 */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_screen_image_viewer, container, false);

        final ImageView imageView = (ImageView) rootView
                .findViewById(R.id.image_viewer_id);
        if (imageUrl.contains("null")) {
            imageView.setBackgroundResource(R.drawable.ic_launcher);
        } else {
            ImageLoader.getInstance()
                    .displayImage(imageUrl, imageView, options);
        }
        uri = Uri.parse(imageUrl);
        String extension = android.webkit.MimeTypeMap
                .getFileExtensionFromUrl(imageUrl.toString());
        mimetype = android.webkit.MimeTypeMap.getSingleton()
                .getMimeTypeFromExtension(extension);

        imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent i = new Intent(getActivity(), ImageOpener.class);
                i.putExtra("IMAGE", imageUrl);
                startActivity(i);

            }
        });


        return rootView;
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
}
