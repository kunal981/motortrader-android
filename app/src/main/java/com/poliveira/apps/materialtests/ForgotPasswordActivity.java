package com.poliveira.apps.materialtests;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONObject;

/**
 * Created by ${ShalviSharma} on 7/31/15.
 */
public class ForgotPasswordActivity extends Activity{
    EditText edttext;
    Button button;
    String emailAddress;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    JSONObject jsonObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        cd = new ConnectionDetector(ForgotPasswordActivity.this);
        isInternetPresent = cd.isConnectingToInternet();
        edttext = (EditText)findViewById(R.id.edt_email_forgot);
        button = (Button)findViewById(R.id.button_submit_forgot);
/**
 * check internet connection
 */
        checkInternet();


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                emailAddress = edttext.getText().toString();

               boolean receive = validation(emailAddress);
                if (receive){
                    new ExecuteForgot().execute(emailAddress);
                }



            }
        });
    }


    private boolean validation(String emailAddress) {
        // TODO Auto-generated method stub
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";

        if (emailAddress.matches("")) {
            LogMessage.showDialog(ForgotPasswordActivity.this, null,
                    "Please enter your email address", "OK");
            return false;
        }
        if (!edttext.getText().toString().matches(emailPattern)) {
            LogMessage.showDialog(ForgotPasswordActivity.this, null,
                    "Please enter valid email address", "OK");
            return false;
        }



        return true;
    }


    class ExecuteForgot extends AsyncTask<String, Integer, String> {


        @Override
        protected String doInBackground(String... params) {
            String result = WSConnector.forgotpwd(params[0]);
            return result;
        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();

           UI.showProgressDialog(ForgotPasswordActivity.this);

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            UI.hideProgressDialog();
            Log.e("result--",""+result);
            if (result.contains("true")){
                edttext.setText("");
               updateUI(result);
            }
            else if (result.contains("false")){

                    LogMessage.showDialog(ForgotPasswordActivity.this, "",
                            "Please check your email address", "OK");

            }
        }
    }
    public void updateUI(String result) {

        try {

            jsonObject = new JSONObject(result);

            String message = jsonObject.getString("message");
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(ForgotPasswordActivity.this);
            alertDialog.setTitle("").setMessage(message).setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            finish();

                        }
                    });

            AlertDialog dialog = alertDialog.create();
            dialog.show();
            TextView messageText = (TextView) dialog
                    .findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER);


        }catch (Exception e){

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(ForgotPasswordActivity.this);
            alertDialog.setTitle("").setMessage("Network connection not found").setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            finish();
                        }
                    });

            AlertDialog dialog = alertDialog.create();
            dialog.show();
            TextView messageText = (TextView) dialog
                    .findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER);

        }
    }
}
