package com.poliveira.apps.materialtests.AddSpareParts;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ${ShalviSharma} on 8/14/15.
 */
public class SpareDetails extends Fragment {

    View rootview;
    EditText edtPrice, edtDescription, edtSpare;
    String idMake, idModel, strSpare, strPrice, strDesc,selectedYear,idLoc;
    Button btnSpareNext;
    ArrayList<String> arrayListName, arrayListIDMake, arrayListModel, arrayListIDModel,arrayListLocation,arrayListIDLocation;
    Spinner spinnermake, spinnermodel, spinneryear,spinnerloc;
    ArrayAdapter<String> adapterMake, adapterMakeModel,adapterYear,adapterLoc;
    JSONObject jsonObject;
    TextView headerTitle;
    ConnectionDetector cd;
    boolean isInternetPresent = false;

    String back ="0";


    public static SpareDetails newInstance() {
        SpareDetails spareDetails = new SpareDetails();
        return spareDetails;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_add_spare_detail, container, false);
        spinnermake = (Spinner) rootview.findViewById(R.id.spinner_spare_make);
        spinnermodel = (Spinner) rootview.findViewById(R.id.spinner_spare_model);
        spinneryear = (Spinner) rootview.findViewById(R.id.spinner_year_spare);
        edtDescription = (EditText) rootview.findViewById(R.id.edt_spare_desc);
        edtPrice = (EditText) rootview.findViewById(R.id.edittext_spare_price);
        edtSpare = (EditText) rootview.findViewById(R.id.edittext_spare_part);
        btnSpareNext = (Button) rootview.findViewById(R.id.b_spare_next);
        spinnerloc = (Spinner) rootview.findViewById(R.id.spinner_spare_state);

        AppCompatActivity activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");
        headerTitle = (TextView) activity.findViewById(R.id.mytext);

        headerTitle.setText("Add Spare Part");
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
/**
 * check internet connection
 */
        checkInternet();

        /*
        Array list
         */


        arrayListName = new ArrayList<>();
        arrayListIDMake = new ArrayList<>();
        arrayListModel = new ArrayList<>();
        arrayListIDModel = new ArrayList<>();
        arrayListLocation = new ArrayList<>();
        arrayListIDLocation = new ArrayList<>();

        String[] year = getResources().getStringArray(R.array.spinnerYear);

        adapterYear = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, year);
        adapterYear.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
        spinneryear.setAdapter(adapterYear);
        spinneryear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedYear = parent.getItemAtPosition(position).toString();



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnSpareNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strSpare = edtSpare.getText().toString();
                strPrice = edtPrice.getText().toString();
                strDesc = edtDescription.getText().toString();
                boolean receive = validation(strSpare,idMake, idModel, selectedYear, idLoc, strPrice, strDesc);
                if (receive) {


                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    SparePictures fragment = new SparePictures();

                    Bundle savedInstanceState = new Bundle();
                    savedInstanceState.putString("MAKE_ID", idMake);
                    savedInstanceState.putString("MODEL_ID", idModel);
                    savedInstanceState.putString("YEAR", selectedYear);
                    savedInstanceState.putString("PRICE", strPrice);
                    savedInstanceState.putString("LOCATION", idLoc);
                    savedInstanceState.putString("DESCRIPTION", strDesc);
                    savedInstanceState.putString("SPARE_PARTS", strSpare);
                    fragmentTransaction.replace(R.id.container, fragment).addToBackStack(fragment.getClass().getName());
                    fragment.setArguments(savedInstanceState);
                    fragmentTransaction.commit();

                }
            }
        });



         /*
        Make API
         */


        new ExecuteMake().execute();


          /*
        Car Location List API
         */


        new ExecuteLocationList().execute();





        rootview.setFocusableInTouchMode(true);
        rootview.requestFocus();
        rootview.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub


                if (keyCode == KeyEvent.KEYCODE_BACK && back.equals("0")) {
                    back = "1";

                    Toast.makeText(getActivity(), "Press again to exit!", Toast.LENGTH_SHORT).show();
                    return true;

                } else {
                    return false;
                }

            }
        });

        return rootview;
    }


     /*
    Car Make Execution
     */


    class ExecuteMake extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String url = WSConnector.getMake();

            return url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            if (result.contains("true")) {


                updateUI(result);
UI.hideProgressDialog();

                if (arrayListName!=null){
                    adapterMake = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, arrayListName);
                    adapterMake.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
                    spinnermake.setAdapter(adapterMake);


                    spinnermake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                            idMake = arrayListIDMake.get(position);



                            new ExecuteModel().execute(idMake);


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
                }



        }
    }


    public void updateUI(String result) {

        try {
            arrayListName.add("Any Makes");
            arrayListIDMake.add("0");

            jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i <= jsonArray.length(); i++) {

                JSONObject jsonInnerObject = jsonArray.getJSONObject(i);

                arrayListIDMake.add(jsonInnerObject.getString("id"));


                arrayListName.add(jsonArray.getJSONObject(i).getString("name"));

            }


        } catch (Exception e) {

        }

    }

     /*
    Car Model Execution
     */


    class ExecuteModel extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String url = WSConnector.postCarModel(params[0]);

            return url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();*/


        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
           /* pDialog.dismiss();*/
            if (result.contains("true")) {
                arrayListIDModel.clear();
                arrayListModel.clear();
                updateModel(result);


                adapterMakeModel = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, arrayListModel);
                adapterMakeModel.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
                spinnermodel.setAdapter(adapterMakeModel);


                spinnermodel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                        idModel = arrayListIDModel.get(position);




                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }
    }

    public void updateModel(String result) {

        try {
            arrayListIDModel.add("0");
            arrayListModel.add("Any Model");


            jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i <= jsonArray.length(); i++) {

                arrayListIDModel.add(jsonArray.getJSONObject(i).getString("id"));

                arrayListModel.add(jsonArray.getJSONObject(i).getString("model"));

            }


        } catch (Exception e) {

        }

    }

 /*
        Location list execution
     */

    class ExecuteLocationList extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            String response = WSConnector.getLocationList();

            return response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.contains("true")) {
                updateLocation(s);
                adapterLoc = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, arrayListLocation);
                adapterLoc.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
                spinnerloc.setAdapter(adapterLoc);
                spinnerloc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        idLoc = arrayListIDLocation.get(position);




                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }
    }

    public void updateLocation(String s) {
        try {
            arrayListIDLocation.add("0");
            arrayListLocation.add("Any State");

            jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("data");


            for (int i = 0; i <= jsonArray.length(); i++) {

                arrayListIDLocation.add(jsonArray.getJSONObject(i).getString("id"));

                arrayListLocation.add(jsonArray.getJSONObject(i).getString("name"));

            }


        } catch (Exception e) {

        }

    }


    private boolean validation(String strSpare,String idMake ,String idModel,String selectedYear,String idLoc, String strPrice, String strDesc) {
        // TODO Auto-generated method stub

        if (strSpare.matches("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter spare part name", "OK");
            return false;
        }


        if (idMake.matches("0")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please select make", "OK");
            return false;
        }

        if (idModel.matches("0")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please select model", "OK");
            return false;
        }

        if (selectedYear.matches("Any Year")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please select year", "OK");
            return false;
        }

        if (idLoc.matches("0")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please select state", "OK");
            return false;
        }


        if (strPrice.matches("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter price", "OK");
            return false;
        }


        if (strDesc.matches("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter description", "OK");
            return false;
        }

        return true;
    }










    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }


}
