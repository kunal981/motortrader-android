package com.poliveira.apps.materialtests;

import android.app.Application;
import android.content.Context;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

/**
 *
 * @author Shalvi Sharma
 *
 */
public class AppController extends Application {

	public static final String TAG = AppController.class.getSimpleName();

	// private RequestQueue mRequestQueue;
	//
	// JSONObject myjsonresponse;
	// String userid;
	//
	// public String getUserid() {
	// return userid;
	// }
	//
	private static AppController mInstance;

	@Override
	public void onCreate() {
		super.onCreate();
		initImageLoader(AppController.this);
		mInstance = this;

	}

	//
	// public RequestQueue getRequestQueue() {
	// if (mRequestQueue == null) {
	// mRequestQueue = Volley.newRequestQueue(getApplicationContext());
	// }
	//
	// return mRequestQueue;
	// }
	//
	// public <T> void addToRequestQueue(Request<T> req, String tag) {
	// // set the default tag if tag is empty
	// int socketTimeout = 12000;// 30 seconds - change to what you want
	// RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
	// DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
	// DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
	// req.setRetryPolicy(policy);
	// req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
	// getRequestQueue().add(req);
	// }
	//
	// public <T> void addToRequestQueue(Request<T> req) {
	//
	// req.setTag(TAG);
	// getRequestQueue().add(req);
	// }
	//
	// public void cancelPendingRequests(Object tag) {
	// if (mRequestQueue != null) {
	// mRequestQueue.cancelAll(tag);
	// }
	// }
	//
	// public void setUserid(String string) {
	// // TODO Auto-generated method stub
	// this.userid = string;
	// }

	public static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you
		// may tune some of them,
		// or you can create default configuration by
		// ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				context).threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
				.diskCacheSize(50 * 1024 * 1024)
				// 50 Mb
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.writeDebugLogs() // Remove for release app
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}

}
