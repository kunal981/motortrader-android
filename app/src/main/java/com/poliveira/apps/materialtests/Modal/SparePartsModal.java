package com.poliveira.apps.materialtests.Modal;

/**
 * Created by ${ShalviSharma} on 8/11/15.
 */
public class SparePartsModal {


    private String seller_num;
    private String company;
    private String model;
    private String city;
    private String price;
    private String image;
    private String year;
    private String id;


    public String getSeller_num() {
        return seller_num;
    }

    public void setSeller_num(String seller_num) {
        this.seller_num = seller_num;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
