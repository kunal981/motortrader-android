package com.poliveira.apps.materialtests.SearchCars;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ${ShalviSharma} on 7/30/15.
 */
public class NewCarFragment extends Fragment {
    Spinner spinnermake, spinnermodel, spinneryear, spinnertrans, spinnerfuel, spinnerbody, spinnerloc;

    Button buttonSearchNew;
    String[] spinnerTrans = {"Any Transmission", "Manual", "Automatic", "Other"};
    String[] spinnerFuel = {"Any Fuel type", "Petrol", "Diesel", "Hybrid", "Other"};
    String back ="0";
    ArrayAdapter<String> adapterMake, adapterMakeModel, adapterYear, adapterTrans, adapterFuel, adapterBody, adapterLoc;

    ArrayList<String> arrayListName, arrayListIDMake, arrayList, arrayListModel, arrayListBodyName, arrayListLocation, arrayListIDModel,
    arrayListIDLocation, arrayListIDBody;
    ConnectionDetector cd;
    boolean isInternetPresent = false;

    JSONObject jsonObject;
    String idMake, idModel, idBody, idLoc, selectedYear;
    int Fuelpos, transpos;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.new_car, container, false);
        spinnermake = (Spinner) rootview.findViewById(R.id.spinner_make);
        spinnermodel = (Spinner) rootview.findViewById(R.id.spinner_model);
        spinneryear = (Spinner) rootview.findViewById(R.id.spinner_year);
        spinnertrans = (Spinner) rootview.findViewById(R.id.spinner_transmission);
        spinnerfuel = (Spinner) rootview.findViewById(R.id.spinner_fuel);
        spinnerbody = (Spinner) rootview.findViewById(R.id.spinner_body);
        spinnerloc = (Spinner) rootview.findViewById(R.id.spinner_location);
        buttonSearchNew = (Button) rootview.findViewById(R.id.b_search);

        arrayList = new ArrayList<>();
        arrayListName = new ArrayList<>();
        arrayListIDMake = new ArrayList<>();
        arrayListModel = new ArrayList<>();
        arrayListBodyName = new ArrayList<>();
        arrayListLocation = new ArrayList<>();
        arrayListIDModel = new ArrayList<>();
        arrayListIDLocation = new ArrayList<>();
        arrayListIDBody = new ArrayList<>();
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
/**
 * check internet connection
 */
        checkInternet();
 /*
        Make API
         */

    new ExecuteMake().execute();


        /*
        Body List API
         */


    new ExecuteBodyList().execute();



        /*
        Car Location List API
         */


    new ExecuteLocationList().execute();





        String[] year = getResources().getStringArray(R.array.spinnerYear);


        adapterYear = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, year);
        adapterYear.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
        spinneryear.setAdapter(adapterYear);
        spinneryear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedYear = parent.getItemAtPosition(position).toString();
                Log.e("string: ", "" + selectedYear);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                LogMessage.showDialog(getActivity(), "", "Please select Year", "OK");

            }
        });


        adapterTrans = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, spinnerTrans);
        adapterTrans.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
        spinnertrans.setAdapter(adapterTrans);
        spinnertrans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                transpos = position;
                Log.e("transpos", "" + transpos);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                LogMessage.showDialog(getActivity(), "", "Please select transmission type", "OK");
            }
        });

        adapterFuel = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, spinnerFuel);
        adapterFuel.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
        spinnerfuel.setAdapter(adapterFuel);

        spinnerfuel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Fuelpos = position;
                Log.e("trans", "" + Fuelpos);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                LogMessage.showDialog(getActivity(), "", "Please select Fuel type", "OK");
            }
        });


        buttonSearchNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SearchCarsFragment fragment = new SearchCarsFragment();
                Bundle savedInstanceState = new Bundle();
                savedInstanceState.putString("MAKE_ID", idMake);
                savedInstanceState.putString("MODEL_ID", idModel);
                savedInstanceState.putString("SELECTED_YEAR", selectedYear);
                savedInstanceState.putString("LOCATION", idLoc);
                savedInstanceState.putString("FUEL_TYPE", "" + Fuelpos);
                savedInstanceState.putString("TRANS_TYPE", "" + transpos);
                savedInstanceState.putString("BODY_TYPE", idBody);

                fragmentTransaction.replace(R.id.container, fragment);

                fragment.setArguments(savedInstanceState);

                fragmentTransaction.commit();
            }
        });


        rootview.setFocusableInTouchMode(true);
        rootview.requestFocus();
        rootview.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub


                if (keyCode == KeyEvent.KEYCODE_BACK && back.equals("0")) {
                    back = "1";

                    Toast.makeText(getActivity(), "Press again to exit the app!", Toast.LENGTH_SHORT).show();
                    return true;

                } else {
                    return false;
                }

            }
        });


        return rootview;
    }





 /*
    Car Make Execution
     */


    class ExecuteMake extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String url = WSConnector.getMake();

            return url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            UI.hideProgressDialog();
            if (result.contains("true")) {


                updateUI(result);

                adapterMake = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, arrayListName);
                adapterMake.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
                spinnermake.setAdapter(adapterMake);


                spinnermake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                        idMake = arrayListIDMake.get(position);
                        Log.e("idMake==", "" + idMake);


                        new ExecuteModel().execute(idMake);


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        LogMessage.showDialog(getActivity(), "", "Please select Make", "OK");
                    }
                });
            }


        }
    }


    public void updateUI(String result) {

        try {
            arrayListName.add("Any Makes");
            arrayListIDMake.add("0");

            jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i <= jsonArray.length(); i++) {

                JSONObject jsonInnerObject = jsonArray.getJSONObject(i);

                arrayListIDMake.add(jsonInnerObject.getString("id"));


                arrayListName.add(jsonArray.getJSONObject(i).getString("name"));

            }


        } catch (Exception e) {

        }

    }


    /*
    Car Model Execution
     */


    class ExecuteModel extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String url = WSConnector.postCarModel(params[0]);

            return url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
*/

        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
           /* pDialog.dismiss();*/
            if (result.contains("true")) {
                arrayListIDModel.clear();
                arrayListModel.clear();
                updateModel(result);


                adapterMakeModel = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, arrayListModel);
                adapterMakeModel.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
                spinnermodel.setAdapter(adapterMakeModel);


                spinnermodel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                        idModel = arrayListIDModel.get(position);

                        Log.e("idModelelse", "" + idModel);


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        LogMessage.showDialog(getActivity(), "", "Please select Model", "OK");
                    }
                });
            }
        }
    }

    public void updateModel(String result) {

        try {
            arrayListIDModel.add("0");
            arrayListModel.add("Any Model");


            jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i <= jsonArray.length(); i++) {

                arrayListIDModel.add(jsonArray.getJSONObject(i).getString("id"));

                arrayListModel.add(jsonArray.getJSONObject(i).getString("model"));

            }


        } catch (Exception e) {

        }

    }



    /*
        Body list execution
     */

    class ExecuteBodyList extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            String url = WSConnector.getBodyList();

            return url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.contains("true")) {

                updateBody(result);


                adapterBody = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, arrayListBodyName);
                adapterBody.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
                spinnerbody.setAdapter(adapterBody);
                spinnerbody.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        idBody = arrayListIDBody.get(position);
                        Log.e("idBody", "" + idBody);


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        LogMessage.showDialog(getActivity(), "", "Please select Body type", "OK");

                    }
                });

            }
        }
    }

    public void updateBody(String result) {

        try {
            arrayListIDBody.add("0");
            arrayListBodyName.add("Any Body Type");

            jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("data");


            for (int i = 0; i <= jsonArray.length(); i++) {


                arrayListIDBody.add(jsonArray.getJSONObject(i).getString("id"));

                arrayListBodyName.add(jsonArray.getJSONObject(i).getString("name"));

            }


        } catch (Exception e) {

        }

    }


     /*
        Location list execution
     */

    class ExecuteLocationList extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            String response = WSConnector.getLocationList();

            return response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.contains("true")) {
                updateLocation(s);
                adapterLoc = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, arrayListLocation);
                adapterLoc.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
                spinnerloc.setAdapter(adapterLoc);
                spinnerloc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        idLoc = arrayListIDLocation.get(position);
                        Log.e("", "" + idLoc);


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        LogMessage.showDialog(getActivity(), "", "Please select Location", "OK");
                    }
                });
            }
        }
    }

    public void updateLocation(String s) {
        try {
            arrayListIDLocation.add("0");
            arrayListLocation.add("Any State");

            jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("data");


            for (int i = 0; i <= jsonArray.length(); i++) {

                arrayListIDLocation.add(jsonArray.getJSONObject(i).getString("id"));

                arrayListLocation.add(jsonArray.getJSONObject(i).getString("name"));

            }


        } catch (Exception e) {

        }

    }
    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }

}
