package com.poliveira.apps.materialtests;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.AppConstant;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONObject;

/**
 * Created by ${ShalviSharma} on 7/31/15.
 */
public class RegisterActivity extends Activity {
    EditText edtfname,edtlname,edtemail,edtpass,edtconfirmpwd,edtAddress;
    String strFname,strLname,strEmail,strPassword,strConfirnPwd,strAddress,uid,message;
    Button buttonRegister;
    ProgressDialog pDialog;
    JSONObject jsonObject;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        /**
         * internet check declaration
         */
        cd = new ConnectionDetector(RegisterActivity.this);

        pDialog = new ProgressDialog(RegisterActivity.this);

        isInternetPresent = cd.isConnectingToInternet();
        sharedPreferences = getSharedPreferences(AppConstant.MOTORTRADER,
                Context.MODE_PRIVATE);



        edtfname = (EditText)findViewById(R.id.edt_fname);
        edtlname = (EditText)findViewById(R.id.edt_lname);

        edtemail = (EditText)findViewById(R.id.edt_email_register);
        edtpass = (EditText)findViewById(R.id.edt_password);
        edtconfirmpwd = (EditText)findViewById(R.id.edt_re_password);
        edtAddress = (EditText)findViewById(R.id.edt_location);

        checkInternet();

        buttonRegister = (Button)findViewById(R.id.button_register);


        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strFname = edtfname.getText().toString();
                strLname = edtlname.getText().toString();

                strEmail = edtemail.getText().toString();

                strAddress = edtAddress.getText().toString();
                strPassword = edtpass.getText().toString();
                strConfirnPwd = edtconfirmpwd.getText().toString();

                boolean receive = validatation_method();
                if (receive && isInternetPresent){




                    new ExecuteRegister()
                            .execute(strFname, strLname,strEmail, strPassword, strAddress);
                }

            }
        });





    }










    private boolean validatation_method() {
        // TODO Auto-generated method stub
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";
        if (edtfname.getText().toString().equals("")) {
            LogMessage.showDialog(RegisterActivity.this, null,
                    "Please enter your first name", "OK");
            return false;
        }
        if (edtlname.getText().toString().equals("")) {
            LogMessage.showDialog(RegisterActivity.this, null,
                    "Please enter your last name ", "OK");
            return false;
        }



        if (edtemail.getText().toString().equals("")) {
            LogMessage.showDialog(RegisterActivity.this, null,
                    "Please enter your email address", "OK");
            return false;
        }

        if (!edtemail.getText().toString().matches(emailPattern)) {
            LogMessage.showDialog(RegisterActivity.this, null,
                    "Please enter valid email address", "OK");
            return false;
        }



        if (edtAddress.getText().toString().equals("")) {
            LogMessage.showDialog(RegisterActivity.this, null,
                    "Please enter your location", "OK");
            return false;
        }

        if (edtpass.getText().toString().equals("")) {
            LogMessage.showDialog(RegisterActivity.this, null,
                    "Please enter your password", "OK");
            return false;
        }


        if (edtconfirmpwd.getText().toString().matches("")) {
            LogMessage.showDialog(RegisterActivity.this, null,
                    "Please again enter your password", "OK");
            return false;
        }

        if (!edtconfirmpwd.getText().toString().trim()
                .matches(edtpass.getText().toString())) {
            LogMessage.showDialog(RegisterActivity.this, null,
                    "Password does not match the re-password",
                    "OK");
            return false;
        }



        return true;
    }





    class ExecuteRegister extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {


            String url = WSConnector.Register(params[0], params[1], params[2], params[3], params[4]);


            return url;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();

           UI.showProgressDialog(RegisterActivity.this);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
           UI.hideProgressDialog();

            if (result.contains("true")) {
                updateUI(result);
            }

            else  if (result.contains("false")){

                LogMessage.showDialog(RegisterActivity.this,"","Email address already exists !!!","OK");

            }
        }
    }


    public void updateUI(String result) {

        try {

            jsonObject = new JSONObject(result);
            uid = jsonObject.getString("userId");
            message = jsonObject.getString("message");
            sharedPrefernces();


            AlertDialog.Builder alertDialog = new AlertDialog.Builder(RegisterActivity.this);
            alertDialog.setTitle("").setMessage(message).setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_right,
                                    R.anim.slide_out_left);
                            finish();

                        }
                    });

            AlertDialog dialog = alertDialog.create();
            dialog.show();
            TextView messageText = (TextView) dialog
                    .findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER);





        }catch (Exception e){

        }

    }




    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(RegisterActivity.this);
            alertDialog.setTitle("").setMessage("Network connection not found").setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            finish();
                        }
                    });

            AlertDialog dialog = alertDialog.create();
            dialog.show();
            TextView messageText = (TextView) dialog
                    .findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER);

        }
    }



    public void sharedPrefernces() {
        editor = sharedPreferences.edit();
        editor.putString(AppConstant.USERID, String.valueOf(uid));



        editor.commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
