package com.poliveira.apps.materialtests.SpareParts;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.ImageHolderSlideFragment;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${ShalviSharma} on 8/3/15.
 */
public class SparePartDetailsFragment extends Fragment {
    Button buttonWrite;
    TextView headerTitle;
    String strSPAREId;
    ViewPager _mViewPager;

    TextView textSellerNo,textSpareName,textPrice,textYear,textLocation,textMake,textModel,textDescription;
    String header,make,model,loc,price,image,phone,state,desc,year,image2,image3,image4;

    JSONObject jsonObject, jsonInnerObject;
    DisplayImageOptions options;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    List<String> arraylistImages;
    ScreenSlidePagerAdapter mPagerAdapter;
    CirclePageIndicator mIndicator;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_spare_details,container,false);
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");
        headerTitle = (TextView) activity.findViewById(R.id.mytext);

        headerTitle.setText("Spare Parts Details");
        if (getArguments()!=null){
            Bundle bundle = this.getArguments();
            strSPAREId = bundle.getString("SPARE_ID");
        }

        textSellerNo = (TextView)rootview.findViewById(R.id.text_seller_number_spare);
        textSpareName = (TextView)rootview.findViewById(R.id.text_spare_name);
        textPrice = (TextView)rootview.findViewById(R.id.text_spare_detail_price);
        textYear = (TextView)rootview.findViewById(R.id.text_spare_detail_date);
        textLocation = (TextView)rootview.findViewById(R.id.text_spare_detail_location);
        textMake = (TextView)rootview.findViewById(R.id.text_spare_detail_make);

        textModel = (TextView)rootview.findViewById(R.id.text_spare_detail_model);
        textDescription = (TextView)rootview.findViewById(R.id.text_spare_detail_desc);

        _mViewPager = (ViewPager)rootview. findViewById(R.id.viewPager);
        mIndicator = (CirclePageIndicator)rootview. findViewById(R.id.indicator);

        arraylistImages = new ArrayList<String>();
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
/**
 * check internet connection
 */
        checkInternet();
 /*






          /*
        API Execution
         */
        if (isInternetPresent){
            new ExecuteSparePartDetails().execute(strSPAREId);
        }





        buttonWrite=(Button)rootview.findViewById(R.id.btn_write_spare_detail);
        buttonWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SpareWritetoUsFragment fragment = new SpareWritetoUsFragment();
                Bundle savedInstanceState = new Bundle();
                savedInstanceState.putString("SPARE_ID", strSPAREId);
                fragmentTransaction.replace(R.id.container, fragment).addToBackStack(null);
                fragment.setArguments(savedInstanceState);
                fragmentTransaction.commit();
            }
        });


        return rootview;
    }


    class ExecuteSparePartDetails extends AsyncTask<String,Integer,String> {

        @Override
        protected String doInBackground(String... params) {

            String URL = WSConnector.post_spareDetails(params[0]);

            return URL;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            UI.hideProgressDialog();
            if (result.contains("true")){
                updateUI(result);


            }
        }
    }
    public void updateUI(String result) {

        try{

            jsonObject = new JSONObject(result);

          jsonInnerObject =  jsonObject.getJSONObject("data");

             header = jsonInnerObject.getString("seller_num");
            make = jsonInnerObject.getString("make");
            model = jsonInnerObject.getString("model");
            year = jsonInnerObject.getString("year");
             loc = jsonInnerObject.getString("location");
             state = jsonInnerObject.getString("state");
            price = jsonInnerObject.getString("price");
            desc =jsonInnerObject.getString("desc");
            image = jsonInnerObject.getString("image");
            phone = jsonInnerObject.getString("phone");

            if (jsonInnerObject.has("image2")){
                image2 = jsonInnerObject.getString("image2");
            }


            if (jsonInnerObject.has("image3")){
                image3 = jsonInnerObject.getString("image3");
            }


            if (jsonInnerObject.has("image4")){
                image4 = jsonInnerObject.getString("image4");
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        if (header!=null){

            textSpareName.setText(header);

        }
        else {
            textSpareName.setText("No value");
        }

        if (phone!=null){
            textSellerNo.setText(phone);
        }
        else {
            textSellerNo.setText("No value");
        }
        if (price!=null){
            textPrice.setText(price);
        }
        else {
            textPrice.setText("No value");
        }

        if (year!=null){
            textYear.setText(year);
        }
        else {
            textYear.setText("No value");
        }



        if (loc!=null){
            textLocation.setText(loc + ","+" "+ state);
        }
        else {
            textLocation.setText("No value");
        }



        if (make!=null){
            textMake.setText(make);
        }
        else {
            textMake.setText("No value");
        }

        if (model!=null){
            textModel.setText(model);
        }
        else {
            textModel.setText("No value");
        }
        if (desc!=null){
            textDescription.setText(desc);
        }
        else {
            textDescription.setText("No value");
        }

        if (image != null && !image.equals("")) {

            arraylistImages.add(image);
        }
        if (image2 != null && !image2.equals("")) {

            arraylistImages.add(image2);
        }
        if (image3 != null && !image3.equals("")) {

            arraylistImages.add(image3);
        }
        if (image4 != null && !image4.equals("")) {

            arraylistImages.add(image4);
        }
        setupViewPager();





    }


    private void setupViewPager() {
        if (arraylistImages.size() == 0) {
        } else {
            mPagerAdapter = new ScreenSlidePagerAdapter(
                    getActivity().getSupportFragmentManager(), arraylistImages);

            _mViewPager.setAdapter(mPagerAdapter);
            mIndicator.setViewPager(_mViewPager);
        }

    }
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        DisplayImageOptions options;

        public ScreenSlidePagerAdapter(FragmentManager fm,
                                       List<String> mArrayList) {
            super(fm);
            options = setupImageLoader();
            mArrayList = arraylistImages;
        }

        @Override
        public Fragment getItem(int position) {

            return ImageHolderSlideFragment.create(position,
                    arraylistImages.get(position), options);
        }

        @Override
        public int getCount() {
            return arraylistImages.size();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }



    private DisplayImageOptions setupImageLoader() {
        return new DisplayImageOptions.Builder().cacheInMemory(true)
                .showImageOnLoading(R.drawable.img_loading)
                .showImageForEmptyUri(R.drawable.no_image_icon)
                .showImageOnFail(R.drawable.no_image_icon)
                .cacheOnDisk(true).considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(1)).build();

    }

    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }


}
