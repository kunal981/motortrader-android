package com.poliveira.apps.materialtests.AddyourCarFragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;

import java.io.File;

/**
 * Created by ${ShalviSharma} on 7/30/15.
 */
public class CarPictureFragment extends Fragment implements View.OnClickListener {
    Button buttonNext;
    ImageView imageView1, imageView2, imageView3;
    String makeid, modelId, yearSearch, strPrice, strDes, bodytypel, transType, fuelType,strLoc;
    public static int RESULT_LOAD_IMAGE = 1;
    public static int RESULT_LOAD_IMAGE2 = 2;
    public static int RESULT_LOAD_IMAGE3 = 3;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE2 = 200;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE3 = 300;
    public static final int MEDIA_TYPE_IMAGE = 1;
    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "Hello Camera";
    private static int RESULT_OK = -1;
    Bitmap bitmap = null;

    Uri selectedImage;
    File destination = null;
    File destination2 = null;
    File destination3 = null;
    String picturePath, picturePath2, picturePath3;
    ConnectionDetector cd;
    boolean isInternetPresent = false;

    public static CarPictureFragment newInstance() {
        CarPictureFragment carPictureFragment = new CarPictureFragment();
        return carPictureFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_car_picture, container, false);


        if (getArguments() != null) {
            Bundle bundle = this.getArguments();
            makeid = bundle.getString("MAKE_ID");
            modelId = bundle.getString("MODEL_ID");
            yearSearch = bundle.getString("SELECTED_YEAR");
            bodytypel = bundle.getString("BODY_TYPE");
            fuelType = bundle.getString("FUEL_TYPE");
            transType = bundle.getString("TRANS_TYPE");
            strPrice = bundle.getString("PRICE");
            strDes = bundle.getString("DESCRIPTION");
            strLoc= bundle.getString("LOCATION");
        }

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        // return view code


        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
/**
 * check internet connection
 */
        checkInternet();

        buttonNext = (Button) rootview.findViewById(R.id.b_pic_next);
        imageView1 = (ImageView) rootview.findViewById(R.id.image1_car);
        imageView2 = (ImageView) rootview.findViewById(R.id.image2_car);
        imageView3 = (ImageView) rootview.findViewById(R.id.image3_car);



        imageView1.setOnClickListener(this);
        imageView2.setOnClickListener(this);
        imageView3.setOnClickListener(this);


        buttonNext.setOnClickListener(new View.OnClickListener() {
            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {

                if (picturePath == null) {
                    LogMessage.showDialog(getActivity(), null,
                            "Please select an image", "OK");

                }
                else {

                    Log.e("picturePath",""+picturePath);
                    Log.e("picturePath2",""+picturePath2);
                    Log.e("picturePath3",""+picturePath3);
                     FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SellerInfoFragment fragment1 = new SellerInfoFragment();
                Bundle savedInstanceState = new Bundle();
                savedInstanceState.putString("MAKE_ID", makeid);
                savedInstanceState.putString("MODEL_ID", modelId);
                savedInstanceState.putString("SELECTED_YEAR", yearSearch);
                savedInstanceState.putString("FUEL_TYPE", "" + fuelType);savedInstanceState.putString("LOCATION", strLoc);
                savedInstanceState.putString("TRANS_TYPE", "" + transType);
                savedInstanceState.putString("BODY_TYPE", bodytypel);
                savedInstanceState.putString("PRICE", strPrice);
                savedInstanceState.putString("DESCRIPTION", strDes);
                savedInstanceState.putString("IMAGE_GALLERY_1", picturePath);
                savedInstanceState.putString("IMAGE_GALLERY_2", picturePath2);
                savedInstanceState.putString("IMAGE_GALLERY_3", picturePath3);


                fragmentTransaction.replace(R.id.container, fragment1).addToBackStack(fragment1.getClass().getName());

                    fragment1.setArguments(savedInstanceState);
                fragmentTransaction.commit();
                }



            }
        });
        return rootview;
    }

    @Override
    public void onClick(View v) {
        int ID = v.getId();
        switch (ID) {
            case R.id.image1_car:
                open_dialog();
                break;

            case R.id.image2_car:
                open_dialog2();
                break;

            case R.id.image3_car:
                open_dialog3();
                break;
        }

    }


    public void open_dialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Choose Image");
        alertDialogBuilder.setPositiveButton("Camera",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        captureImage();

                    }
                });
        alertDialogBuilder.setNegativeButton("Gallery",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(i, RESULT_LOAD_IMAGE);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


    /*
         * Capturing Camera Image will lauch camera app requrest image capture
         */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        picturePath = String.valueOf(new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg"));

        destination = new File(picturePath);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                && null != data) {
            selectedImage = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);

            cursor.close();

            try {
                // hide video preview
                BitmapFactory.Options options;

                options = new BitmapFactory.Options();
                options.inSampleSize = 2;

                bitmap = BitmapFactory.decodeFile(picturePath,options);


                imageView1.setImageBitmap(bitmap);

               /* File file = new File(picturePath);
                Log.e("file: ", "" + file);*/


                // Bitmap mBitmap_send = StringToBitMap(picturePath);
                // Log.e("mBitmap_send: ", ""+mBitmap_send);


            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        } else if (requestCode == RESULT_LOAD_IMAGE2 && resultCode == RESULT_OK
                && null != data) {

            selectedImage = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath2 = cursor.getString(columnIndex);

            cursor.close();

            try {
                // hide video preview
                BitmapFactory.Options options;

                options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                bitmap = BitmapFactory.decodeFile(picturePath2,options);


                imageView2.setImageBitmap(bitmap);

               /* File file2 = new File(picturePath);
                Log.e("file2: ", "" + file2);*/


                // Bitmap mBitmap_send = StringToBitMap(picturePath);
                // Log.e("mBitmap_send: ", ""+mBitmap_send);


            } catch (NullPointerException e) {
                e.printStackTrace();
            }


        } else if (requestCode == RESULT_LOAD_IMAGE3 && resultCode == RESULT_OK
                && null != data) {

            selectedImage = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath3 = cursor.getString(columnIndex);

            cursor.close();

            try {
                // hide video preview
                BitmapFactory.Options options;

                options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                bitmap = BitmapFactory.decodeFile(picturePath3,options);


                imageView3.setImageBitmap(bitmap);

               /* File file2 = new File(picturePath);
                Log.e("file2: ", "" + file2);*/


                // Bitmap mBitmap_send = StringToBitMap(picturePath);
                // Log.e("mBitmap_send: ", ""+mBitmap_send);


            } catch (NullPointerException e) {
                e.printStackTrace();
            }


        } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            BitmapFactory.Options options;

            options = new BitmapFactory.Options();
            options.inSampleSize = 2;

            Bitmap bitmap = BitmapFactory.decodeFile(
                    destination.getAbsolutePath(), options);

            imageView1.setImageBitmap(bitmap);

        } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE2) {
            BitmapFactory.Options options;

            options = new BitmapFactory.Options();
            options.inSampleSize = 2;

            Bitmap bitmap = BitmapFactory.decodeFile(
                    destination2.getAbsolutePath(), options);

            imageView2.setImageBitmap(bitmap);

        } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE3) {
            BitmapFactory.Options options;

            options = new BitmapFactory.Options();
            options.inSampleSize = 2;

            Bitmap bitmap = BitmapFactory.decodeFile(
                    destination3.getAbsolutePath(), options);

            imageView3.setImageBitmap(bitmap);

        }

    }

    public void open_dialog2() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Choose Image");
        alertDialogBuilder.setPositiveButton("Camera",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        captureImage2();

                    }
                });
        alertDialogBuilder.setNegativeButton("Gallery",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(i, RESULT_LOAD_IMAGE2);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


    public void open_dialog3() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Choose Image");
        alertDialogBuilder.setPositiveButton("Camera",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        captureImage3();

                    }
                });
        alertDialogBuilder.setNegativeButton("Gallery",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(i, RESULT_LOAD_IMAGE3);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    /*
            * Capturing Camera Image will lauch camera app requrest image capture
            */
    private void captureImage2() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);



        picturePath2 = String.valueOf(new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg"));

        destination2 = new File(picturePath2);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination2));
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE2);
    }

    /*
            * Capturing Camera Image will lauch camera app requrest image capture
            */
    private void captureImage3() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


        picturePath3 = String.valueOf(new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg"));

        destination3 = new File(picturePath3);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination3));
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE3);
    }

    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }

}
