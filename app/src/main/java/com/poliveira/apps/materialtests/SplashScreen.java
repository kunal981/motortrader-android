package com.poliveira.apps.materialtests;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.widget.TextView;

import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.AppConstant;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONObject;

/**
 * Created by ${ShalviSharma} on 7/29/15.
 */
public class SplashScreen extends Activity {
    String username, password, id;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ConnectionDetector cd;
    boolean isInternetPresent = false;

    JSONObject jsonObject;
    private int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferences = getSharedPreferences(AppConstant.MOTORTRADER,
                Context.MODE_PRIVATE);
        cd = new ConnectionDetector(SplashScreen.this);

        isInternetPresent = cd.isConnectingToInternet();
        checkInternet();


        new Handler().postDelayed(new Runnable() {

			/*
             * Showing splash screen with a timer.
			 */

            @Override
            public void run() {
                if (sharedPreferences != null && sharedPreferences.contains(AppConstant.USERNAME)
                        && sharedPreferences.contains(AppConstant.PASSWORD)) {
                    username = sharedPreferences.getString(
                            AppConstant.USERNAME, "");
                    password = sharedPreferences.getString(
                            AppConstant.PASSWORD, "");
                    id = sharedPreferences.getString(
                            AppConstant.USERID, "");



                    if (isInternetPresent){
                        new ExecuteLogin().execute(username, password);
                    }





                } else {
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.slide_in_right,
                            R.anim.slide_out_left);
                    finish();

                }

            }
        }, SPLASH_TIME_OUT);
    }


    class ExecuteLogin extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String result = WSConnector.login(params[0], params[1]);
            return result;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
            UI.showProgressDialog(SplashScreen.this);

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
           UI.hideProgressDialog();


            if (result.contains("true") && isInternetPresent) {

                updateUI(result);
            } else {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(SplashScreen.this);
                alertDialog.setTitle("").setMessage("Invalid login details").setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                                finish();
                            }
                        });

                AlertDialog dialog = alertDialog.create();
                dialog.show();
                TextView messageText = (TextView) dialog
                        .findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER);
            }


        }
    }


    public void updateUI(String result) {

        try {

            jsonObject = new JSONObject(result);

            String uid = jsonObject.getString("userId");

            if (uid != null && uid.matches(id)) {
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right,
                        R.anim.slide_out_left);
                finish();
            } else {

            }


        } catch (Exception e) {
            e.getMessage();
        }

    }



    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(SplashScreen.this);
            alertDialog.setTitle("").setMessage("Network connection not found").setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            finish();
                        }
                    });

            AlertDialog dialog = alertDialog.create();
            dialog.show();
            TextView messageText = (TextView) dialog
                    .findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER);

        }
    }

}

