package com.poliveira.apps.materialtests.SpareParts;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.poliveira.apps.materialtests.AddSpareParts.SpareDetails;
import com.poliveira.apps.materialtests.Modal.SparePartsModal;
import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.AppConstant;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${ShalviSharma} on 7/29/15.
 */
public class SparePartsFragment extends Fragment {
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    ListView listView;
    Adapter adapter;
    private boolean mSearchCheck;
    String userid;
    SharedPreferences sharedPreferences;
    List<SparePartsModal> sparePartsModalsList;
    String strNum,strCompany,stryear,strmodel,strcity,strprice,strImage;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    String back ="0";
    JSONObject jsonObject;
    String id,seller_num,company,year,model,city,price,image;
    ViewHolder holder;
    TextView headerTitle;
    public static SparePartsFragment newInstance(String text) {

        SparePartsFragment mFragment = new SparePartsFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



      /*  userid = sharedPreferences.getString(
                AppConstant.USERID, "");*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_spare, container, false);
        sharedPreferences = getActivity().getSharedPreferences(AppConstant.MOTORTRADER,
                Context.MODE_PRIVATE);
        listView = (ListView) rootview.findViewById(R.id.listview_spare_parts);

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        AppCompatActivity activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");
        headerTitle = (TextView) activity.findViewById(R.id.mytext);

        headerTitle.setText("Spare Parts");
/**
 * check internet connection
 */
        checkInternet();
 /*

        /*
        Api execution
         */
        new ExecuteSpareParts().execute(userid);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final SparePartsModal partsModal = sparePartsModalsList.get(position);
                String spareid = partsModal.getId();


                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SparePartDetailsFragment fragment = new SparePartDetailsFragment();
                Bundle savedInstanceState = new Bundle();
                savedInstanceState.putString("SPARE_ID", spareid);
                fragmentTransaction.replace(R.id.container, fragment).addToBackStack(null);
                fragment.setArguments(savedInstanceState);
                fragmentTransaction.commit();

            }
        });

        rootview.setFocusableInTouchMode(true);
        rootview.requestFocus();
        rootview.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub


                if (keyCode == KeyEvent.KEYCODE_BACK && back.equals("0")) {
                    back = "1";

                    Toast.makeText(getActivity(), "Press again to exit the app!", Toast.LENGTH_SHORT).show();
                    return true;

                } else {
                    return false;
                }

            }
        });



        return rootview;
    }


    private DisplayImageOptions setupImageLoader() {
        return new DisplayImageOptions.Builder().cacheInMemory(true)
                .showImageOnLoading(R.drawable.img_loading)
                .showImageForEmptyUri(R.drawable.no_image_icon)
                .showImageOnFail(R.drawable.no_image_icon)
                .cacheOnDisk(true).considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(1)).build();

    }


    class Adapter extends BaseAdapter {
        List<SparePartsModal> sparePartsModalsList;
        private Context context;
        private LayoutInflater inflater;
        DisplayImageOptions options;
        public Adapter(Context context, List<SparePartsModal> sparePartsModalsList) {
            this.context = context;
            this.sparePartsModalsList = sparePartsModalsList;
            options = setupImageLoader();
        }

        @Override
        public int getCount() {
            return sparePartsModalsList.size();
        }

        @Override
        public Object getItem(int position) {
            return sparePartsModalsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {


            if (inflater == null) {
                inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            }


            if (convertView == null) {
                convertView = inflater.inflate(R.layout.spare_item_list, null);
                holder = new ViewHolder();
                holder.imageView = (ImageView)convertView.findViewById(R.id.img_spare_parts);
                holder.txtHeading = (TextView)convertView.findViewById(R.id.txt_spare_title);
                holder.txtPrice = (TextView)convertView.findViewById(R.id.txt_spare_price);
                holder.txtNumber = (TextView)convertView.findViewById(R.id.txt_spare_number);
                holder.txtMAke = (TextView)convertView.findViewById(R.id.txt_make);
                holder.txtlocation = (TextView)convertView.findViewById(R.id.txt_location_spare);
                holder.txtyear = (TextView)convertView.findViewById(R.id.txt_year_spare);
                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }


            final  SparePartsModal partsModal = sparePartsModalsList.get(position);

            strmodel=partsModal.getModel();

            if (strmodel.contains("null")){

                holder.txtHeading.setText("No value");
            }
            else {
                holder.txtHeading.setText(strmodel);
            }

            strprice =partsModal.getPrice();

            if (strprice.contains("null")){

                holder.txtPrice.setText("No value");
            }
            else {
                holder.txtPrice.setText(strprice);
            }

            strNum = partsModal.getSeller_num();
            if (strNum.contains("null") ){
                holder.txtNumber.setText("No value");

            }

            else {
                holder.txtNumber.setText(strNum);
            }


            strCompany =partsModal.getCompany();
            if (strCompany.contains("null")){
                holder.txtMAke.setText("No value");
            }
            else {

                holder.txtMAke.setText(strCompany);
            }


            strcity = partsModal.getCity();
            if (strcity.contains("null")){
                holder.txtlocation.setText("No value");
            }
            else {

                holder.txtlocation.setText(strcity);
            }

            stryear = partsModal.getYear();
            if (stryear.contains("null")){
                holder.txtyear.setText("No value");
            }
            else {

                holder.txtyear.setText(stryear);
            }

            image = partsModal.getImage();
            if (image!=null){
                ImageLoader.getInstance().displayImage(image, holder.imageView, options);
            }



            return convertView;
        }
    }














    class ExecuteSpareParts extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {

            String url = WSConnector.getSparePart();

            return url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            UI.hideProgressDialog();
            if (result.contains("true")) {
                sparePartsModalsList = updateUI(result);
                adapter=new Adapter(getActivity(),sparePartsModalsList);
                listView.setAdapter(adapter);

            }
            else if (result.contains("false")){
                LogMessage.showDialog(getActivity(), "", "No spare parts added", "OK");
            }
        }


    }

    private List<SparePartsModal> updateUI(String result) {


        List<SparePartsModal> sparePartsModalsList = new ArrayList<SparePartsModal>();
        try {
            jsonObject = new JSONObject(result);

            JSONArray jsonArray = jsonObject.getJSONArray("data");


            for (int i=0;i<=jsonArray.length();i++){

                SparePartsModal partsModal = new SparePartsModal();
                JSONObject jsonInnerObject = jsonArray.getJSONObject(i);

                id = jsonInnerObject.getString("id");
                seller_num = jsonInnerObject.getString("phone");
                company = jsonInnerObject.getString("company");
                year = jsonInnerObject.getString("year");
                model = jsonInnerObject.getString("model");
                city = jsonInnerObject.getString("city");
                price = jsonInnerObject.getString("price");
                image = jsonInnerObject.getString("image");

                partsModal.setId(id);
                partsModal.setSeller_num(seller_num);
                partsModal.setCompany(company);
                partsModal.setYear(year);
                partsModal.setModel(model);
                partsModal.setCity(city);
                partsModal.setPrice(price);
                partsModal.setImage(image);

                sparePartsModalsList.add(partsModal);





            }

        } catch (Exception e) {

        }
        return sparePartsModalsList;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);


        final MenuItem menuItem = menu.findItem(R.id.menu_search);
        menuItem.setVisible(false);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint(this.getString(R.string.search));

        ((EditText) searchView.findViewById(R.id.search_src_text))
                .setHintTextColor(Color.WHITE);
        searchView.setOnQueryTextListener(onQuerySearchView);

        menu.findItem(R.id.menu_add).setVisible(true);

        mSearchCheck = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        FragmentManager mFragmentManager = getActivity().getSupportFragmentManager();
        switch (item.getItemId()) {

            case R.id.menu_add:
                //Toast.makeText(getActivity(), R.string.add, Toast.LENGTH_SHORT).show();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SpareDetails fragment = new SpareDetails();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
                break;

            case R.id.menu_search:
                mSearchCheck = true;
                //Toast.makeText(getActivity(), R.string.search, Toast.LENGTH_SHORT).show();

                break;
        }
        return true;
    }

    private SearchView.OnQueryTextListener onQuerySearchView = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            if (mSearchCheck) {
                // implement your search here
            }
            return false;
        }
    };

    static class ViewHolder {
        ImageView imageView;
        TextView txtHeading, txtPrice, txtNumber, txtMAke, txtlocation,
                txtyear;

    }


    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }
}
