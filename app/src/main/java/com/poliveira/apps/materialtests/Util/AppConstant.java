package com.poliveira.apps.materialtests.Util;

/**
 * Created by ${ShalviSharma} on 8/10/15.
 */
public class AppConstant {

    public static final String MOTORTRADER = "Motortrader";
    public static final String USERID = "userId";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";



    public static final String MOTO = "http://www.motortrader.ng/api/";
    public static final String QUICKTELLER = "http://www.motortrader.ng/quick_teller_payment/";
    public static final String LOGIN = "login.php";
    public static final String FORGOTPASSWORD = "forgotpass.php";
    public static final String REGISTER = "register.php";
    public static final String CARLIST = "carlisting.php";
    public static final String CARDETAILS = "carDetails.php";
    public static final String RESETPASSWORD = "resetpassword.php";
    public static final String WRITETOUS = "enquiryform.php";
    public static final String SPAREPARTS = "sparepartslisting.php";
    public static final String MAKELISTING = "findmake.php";
    public static final String CARMODEL = "findModel.php";
    public static final String BODYLIST = "bodylist.php";
    public static final String CARLOCATION = "locationlist.php";
    public static final String ADVANCE_SEARCH = "carSearch.php";
    public static final String CAR_POST = "car_post.php";
    public static final String SPARE_POST = "spare_post.php";
    public static final String SPAREDETAILS = "parts_detail.php";
    public static final String SPARE_WRITE = "spare_enquiry.php";
    public static final String CHECK_PAYMENT = "check_payment_status.php";




}
