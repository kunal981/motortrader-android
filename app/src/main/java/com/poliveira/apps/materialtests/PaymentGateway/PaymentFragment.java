package com.poliveira.apps.materialtests.PaymentGateway;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.poliveira.apps.materialtests.AddyourCarFragment.AddCarDetailsFragment;
import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.Util.AppConstant;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONObject;

/**
 * Created by ${ShalviSharma} on 10/23/15.
 */
public class PaymentFragment extends Fragment {
    TextView headerTitle;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String user_id;
    JSONObject jsonObject;
int CANCEL=0;

    public static PaymentFragment newInstance() {
        PaymentFragment paymentFragment = new PaymentFragment();
        return paymentFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_payment, container, false);

        AppCompatActivity activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");
        headerTitle = (TextView) activity.findViewById(R.id.mytext);

        headerTitle.setText("Add your Car");

        sharedPreferences = getActivity().getSharedPreferences(AppConstant.MOTORTRADER,
                Context.MODE_PRIVATE);

        user_id = sharedPreferences.getString(AppConstant.USERID, "");


        new ExecutePaymentResponse().execute(user_id);

        return rootview;


    }


    class ExecutePaymentResponse extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String response = WSConnector.postPaymentResponse(params[0]);

            Log.e("response", "" + response);

            return response;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           // UI.showProgressDialog(getActivity());
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
           // UI.hideProgressDialog();

            if (result.contains("false")) {


                resultPayment(result);


            } else {

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                AddCarDetailsFragment fragment = new AddCarDetailsFragment();


                fragmentTransaction.replace(R.id.container, fragment);

                fragmentTransaction.commit();
            }
        }

        private void resultPayment(String result) {

            try {
                jsonObject = new JSONObject(result);
                String message = jsonObject.getString("message");

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Vechicle Listing Payment").setMessage(message).setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // TODO Auto-generated method stub

                                        Intent intentPayment = new Intent();
                                        intentPayment.setAction(Intent.ACTION_VIEW);
                                        intentPayment.addCategory(Intent.CATEGORY_BROWSABLE);
                                        intentPayment.setData(Uri
                                                .parse("https://www.motortrader.ng/car_post_payment.php"));
                                        startActivityForResult(intentPayment,CANCEL);

                                    }
                                }


                        );


                AlertDialog dialog = alertDialog.create();
                dialog.show();
                TextView messageText = (TextView) dialog
                        .findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER);


            } catch (Exception e) {

            }


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
