package com.poliveira.apps.materialtests.SearchCars;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.ImageHolderSlideFragment;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;
import com.poliveira.apps.materialtests.fragment.WritetoUsFragment;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${ShalviSharma} on 8/3/15.
 */
public class SearchCarDetailsFragment extends Fragment {
    Button buttonWrite;
    ViewPager _mViewPager;
    TextView headerTitle,textSellerNo,textCarName,textPrice,textYear,textLocation,textFuel,textTrans,textBody,textDescription;
    ImageView imageViewCar;

    String uuid;
    String seller_num,make,model,fuel_type,price,image,state,location,transmission,body_type,desc,year,image2,image3,image4;

    JSONArray jsonArray;
    JSONObject jsonObject, jsonInnerObject;
    DisplayImageOptions options;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    List<String> arraylistImages;
    ScreenSlidePagerAdapter mPagerAdapter;
    CirclePageIndicator mIndicator;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_search_car_details,container,false);

        AppCompatActivity activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");
        headerTitle = (TextView) activity.findViewById(R.id.mytext);

        headerTitle.setText("Search Car Details");


        textSellerNo = (TextView)rootview.findViewById(R.id.text_search_car_number);
        textCarName = (TextView)rootview.findViewById(R.id.text_car_name_search);
        textPrice = (TextView)rootview.findViewById(R.id.text_car_price_search_car);
        textYear = (TextView)rootview.findViewById(R.id.text_date_search_car);
        textLocation = (TextView)rootview.findViewById(R.id.text_location_search_car);
        textFuel = (TextView)rootview.findViewById(R.id.text_fuel_search_car);
        textTrans = (TextView)rootview.findViewById(R.id.text_transmission_search_car);
        textBody = (TextView)rootview.findViewById(R.id.text_body_type_search_car);
        textDescription = (TextView)rootview.findViewById(R.id.text_desc_search_car);

        buttonWrite=(Button)rootview.findViewById(R.id.btn_write_search_car);


        _mViewPager = (ViewPager)rootview. findViewById(R.id.viewPager);
        mIndicator = (CirclePageIndicator)rootview. findViewById(R.id.indicator);
        arraylistImages = new ArrayList<String>();


        if (getArguments()!=null){
            Bundle bundle = this.getArguments();
            uuid = bundle.getString("ID");
        }



        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
/**
 * check internet connection
 */
        checkInternet();
        /*
        API Execution
         */
        new ExecuteCarDetails().execute(uuid);


        buttonWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                WritetoUsFragment fragment = new WritetoUsFragment();
                Bundle savedInstanceState = new Bundle();
                savedInstanceState.putString("CAR_ID", uuid);
                fragmentTransaction.replace(R.id.container, fragment).addToBackStack(null);
                fragment.setArguments(savedInstanceState);
                fragmentTransaction.commit();
            }
        });

        return rootview;
    }


    class ExecuteCarDetails extends AsyncTask<String,Integer,String> {

        @Override
        protected String doInBackground(String... params) {

            String URL = WSConnector.post_carDetails(params[0]);

            return URL;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            UI.hideProgressDialog();
            if (result.contains("true")){
                updateUI(result);
            }
        }
    }



    public void updateUI(String result) {

        try{
            jsonObject = new JSONObject(result);

            jsonInnerObject =  jsonObject.getJSONObject("data");


            fuel_type = jsonInnerObject.getString("fuel_type");
            model = jsonInnerObject.getString("model");
            seller_num = jsonInnerObject.getString("seller_num");
            desc = jsonInnerObject.getString("desc");
            price = jsonInnerObject.getString("price");
            body_type = jsonInnerObject.getString("body_type");
            location = jsonInnerObject.getString("location");
            state=jsonInnerObject.getString("state");
            image = jsonInnerObject.getString("image");
            year = jsonInnerObject.getString("year");
            transmission = jsonInnerObject.getString("transmission");
            make =jsonInnerObject.getString("make");

            if (jsonInnerObject.has("image2")){
                image2 = jsonInnerObject.getString("image2");
            }


            if (jsonInnerObject.has("image3")){
                image3 = jsonInnerObject.getString("image3");
            }


            if (jsonInnerObject.has("image4")){
                image4 = jsonInnerObject.getString("image4");
            }


        }catch (Exception e){
            e.printStackTrace();
        }




        textSellerNo.setText(seller_num);
        textCarName.setText(make + " " + model);
        textPrice.setText(price);
        textYear.setText(year);
        textLocation.setText(location);
        textFuel.setText(fuel_type);
        textTrans.setText(transmission);
        textBody.setText(body_type);
        textDescription.setText(desc);

        if (image != null && !image.equals("")) {

            arraylistImages.add(image);
        }
        if (image2 != null && !image2.equals("")) {

            arraylistImages.add(image2);
        }
        if (image3 != null && !image3.equals("")) {

            arraylistImages.add(image3);
        }
        if (image4 != null && !image4.equals("")) {

            arraylistImages.add(image4);
        }
        setupViewPager();





    }
    private void setupViewPager() {
        if (arraylistImages.size() == 0) {
        } else {
            mPagerAdapter = new ScreenSlidePagerAdapter(
                    getActivity().getSupportFragmentManager(), arraylistImages);

            _mViewPager.setAdapter(mPagerAdapter);
            mIndicator.setViewPager(_mViewPager);
        }

    }
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        DisplayImageOptions options;

        public ScreenSlidePagerAdapter(FragmentManager fm,
                                       List<String> mArrayList) {
            super(fm);
            options = setupImageLoader();
            mArrayList = arraylistImages;
        }

        @Override
        public Fragment getItem(int position) {

            return ImageHolderSlideFragment.create(position,
                    arraylistImages.get(position), options);
        }

        @Override
        public int getCount() {
            return arraylistImages.size();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    private DisplayImageOptions setupImageLoader() {
        return new DisplayImageOptions.Builder().cacheInMemory(true)
                .showImageOnLoading(R.drawable.img_loading)
                .showImageForEmptyUri(R.drawable.no_image_icon)
                .showImageOnFail(R.drawable.no_image_icon)
                .cacheOnDisk(true).considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(1)).build();

    }

    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }
}
