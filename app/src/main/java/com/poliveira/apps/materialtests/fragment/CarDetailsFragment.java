package com.poliveira.apps.materialtests.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.ImageHolderSlideFragment;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ${ShalviSharma} on 7/28/15.
 */
public class CarDetailsFragment extends Fragment {
    Button buttonWrite;
    ViewPager _mViewPager;
    TextView headerTitle, textSellerNo, textCarName, textPrice, textYear, textLocation, textFuel, textTrans, textBody, textDescription;
    ImageView imageViewCar;

    String uuid;
    String seller_num, make, model, fuel_type, price, image, state, location, transmission, body_type, desc, year,image2,image3,image4;
    CirclePageIndicator mIndicator;

    JSONObject jsonObject, jsonInnerObject;
    DisplayImageOptions options;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    List<String> arraylistImages;
    ScreenSlidePagerAdapter mPagerAdapter;





    public static CarDetailsFragment newInstance() {
        CarDetailsFragment carDetailsFragment = new CarDetailsFragment();
        return carDetailsFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_cardetails, container, false);
        textSellerNo = (TextView) rootview.findViewById(R.id.text_seller_number_);
        textCarName = (TextView) rootview.findViewById(R.id.text_car_name_);
        textPrice = (TextView) rootview.findViewById(R.id.text_car_price_);
        textYear = (TextView) rootview.findViewById(R.id.text_date_);
        textLocation = (TextView) rootview.findViewById(R.id.text_location_);
        textFuel = (TextView) rootview.findViewById(R.id.text_fuel_);
        textTrans = (TextView) rootview.findViewById(R.id.text_transmission_);
        textBody = (TextView) rootview.findViewById(R.id.text_body_type_);
        textDescription = (TextView) rootview.findViewById(R.id.text_desc_);


        buttonWrite = (Button) rootview.findViewById(R.id.btn_write);
        _mViewPager = (ViewPager)rootview. findViewById(R.id.viewPager);
        mIndicator = (CirclePageIndicator)rootview. findViewById(R.id.indicator);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");
        headerTitle = (TextView) activity.findViewById(R.id.mytext);

        headerTitle.setText("Car Details");
        options = setupImageLoader();

        if (getArguments() != null) {
            Bundle bundle = this.getArguments();
            uuid = bundle.getString("UUID_PASSED");
        }

        arraylistImages = new ArrayList<String>();

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();


/**
 * check internet connection
 */
        checkInternet();
        /*
        API Execution
         */
        new ExecuteCarDetails().execute(uuid);


        buttonWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                WritetoUsFragment fragment = new WritetoUsFragment();
                Bundle savedInstanceState = new Bundle();
                savedInstanceState.putString("CAR_ID", uuid);
                fragmentTransaction.replace(R.id.container, fragment).addToBackStack(null);
                fragment.setArguments(savedInstanceState);
                fragmentTransaction.commit();
            }
        });


        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViewPager();
    }



    class ExecuteCarDetails extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            String URL = WSConnector.post_carDetails(params[0]);

            return URL;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            UI.hideProgressDialog();
            Log.e("result",""+result);
            if (result.contains("true")) {
                updateUI(result);
            }
        }
    }


    public void updateUI(String result) {

        try {
            jsonObject = new JSONObject(result);

            jsonInnerObject = jsonObject.getJSONObject("data");


            fuel_type = jsonInnerObject.getString("fuel_type");
            model = jsonInnerObject.getString("model");
            seller_num = jsonInnerObject.getString("seller_num");
            desc = jsonInnerObject.getString("desc");
            price = jsonInnerObject.getString("price");
            body_type = jsonInnerObject.getString("body_type");
            location = jsonInnerObject.getString("location");
            state = jsonInnerObject.getString("state");
            image = jsonInnerObject.getString("image");
            year = jsonInnerObject.getString("year");
            transmission = jsonInnerObject.getString("transmission");
            make = jsonInnerObject.getString("make");


            if (jsonInnerObject.has("image2")){
                image2 = jsonInnerObject.getString("image2");
            }


            if (jsonInnerObject.has("image3")){
                image3 = jsonInnerObject.getString("image3");
            }


            if (jsonInnerObject.has("image4")){
                image4 = jsonInnerObject.getString("image4");
            }



        } catch (Exception e) {
            e.printStackTrace();
        }


        if (seller_num != null) {
            textSellerNo.setText(seller_num);
        } else {
            textSellerNo.setText("No value");
        }

        if (make != null && model != null) {
            textCarName.setText(make + " " + model);
        } else {
            textCarName.setText("No value");
        }
        if (price != null) {
            textPrice.setText(price);
        } else {
            textPrice.setText("No value");
        }
        if (year != null) {
            textYear.setText(year);
        } else {
            textYear.setText("No value");
        }

        if (location != null && state != null) {
            textLocation.setText(location + "," + " " + state);
        } else {
            textLocation.setText("No value");
        }

        if (fuel_type != null) {
            textFuel.setText(fuel_type);
        } else {
            textFuel.setText("No value");
        }

        if (transmission != null) {
            textTrans.setText(transmission);
        } else {
            textTrans.setText("No value");
        }
        if (body_type != null) {
            textBody.setText(body_type);
        } else {
            textBody.setText("No value");
        }

        if (desc != null) {
            textDescription.setText(desc);
        } else {
            textDescription.setText("No value");
        }

        if (image != null && !image.equals("")) {

            arraylistImages.add(image);
        }
        if (image2 != null && !image2.equals("")) {

            arraylistImages.add(image2);
        }
        if (image3 != null && !image3.equals("")) {

            arraylistImages.add(image3);
        }
        if (image4 != null && !image4.equals("")) {

            arraylistImages.add(image4);
        }
        setupViewPager();

    }
    private void setupViewPager() {
        if (arraylistImages.size() == 0) {
            Log.e("arraylistImages.sizeif",""+arraylistImages.size());
            mPagerAdapter = new ScreenSlidePagerAdapter(
                    getActivity().getSupportFragmentManager(), arraylistImages);

            _mViewPager.setAdapter(mPagerAdapter);
            mIndicator.setViewPager(_mViewPager);
        } else {
            Log.e("sizeelse",""+arraylistImages.size());
            mPagerAdapter = new ScreenSlidePagerAdapter(
                    getActivity().getSupportFragmentManager(), arraylistImages);

            _mViewPager.setAdapter(mPagerAdapter);
            mIndicator.setViewPager(_mViewPager);
        }

    }

    private DisplayImageOptions setupImageLoader() {
        return new DisplayImageOptions.Builder().cacheInMemory(true)
                .showImageOnLoading(R.drawable.img_loading)
                .showImageForEmptyUri(R.drawable.no_image_icon)
                .showImageOnFail(R.drawable.no_image_icon)
                .cacheOnDisk(true).considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(1)).build();

    }




    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        DisplayImageOptions options;

        public ScreenSlidePagerAdapter(FragmentManager fm,
                                       List<String> mArrayList) {
            super(fm);
            options = setupImageLoader();
            mArrayList = arraylistImages;
        }

        @Override
        public Fragment getItem(int position) {

            return ImageHolderSlideFragment.create(position,
                    arraylistImages.get(position), options);
        }

        @Override
        public int getCount() {
            return arraylistImages.size();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onPause() {
        super.onPause();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }




    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }

}
