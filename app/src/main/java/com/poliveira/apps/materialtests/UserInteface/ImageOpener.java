package com.poliveira.apps.materialtests.UserInteface;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.poliveira.apps.materialtests.R;

/**
 * Created by ${ShalviSharma} on 8/27/15.
 */
public class ImageOpener extends Activity {

    String image;
    ImageView imageView;
    static DisplayImageOptions options;
    RelativeLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_opener);
        image = getIntent().getExtras().getString("IMAGE");
        options = setupImageLoader();
        imageView = (ImageView) findViewById(R.id.image_opener);

        layout = (RelativeLayout) findViewById(R.id.relative_layout);
        ImageLoader.getInstance().displayImage(image, imageView, options);

        layout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();

            }
        });

        Log.e("image-->", "" + image);
    }

    private DisplayImageOptions setupImageLoader() {
        return new DisplayImageOptions.Builder().cacheInMemory(true)
                .showImageOnLoading(R.drawable.img_loading)
                .showImageForEmptyUri(R.drawable.no_image_icon)
                .showImageOnFail(R.drawable.no_image_icon)
                .cacheOnDisk(true).considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(1)).build();

    }
}
