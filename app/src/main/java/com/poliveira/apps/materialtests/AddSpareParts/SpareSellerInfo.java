package com.poliveira.apps.materialtests.AddSpareParts;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.poliveira.apps.materialtests.PostAPi;
import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.AppConstant;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by ${ShalviSharma} on 8/17/15.
 */
public class SpareSellerInfo extends Fragment {

    EditText edtname, edtemail, edtnumber, edtAddress;
    Button btnSubmit;
    String strname, stremail, strnumber, straddress, userID, strSpare;
    SharedPreferences sharedPreferences;
    String makeid, modelId, yearSearch, strPrice, strDes, strLocation, strYear, gallery1, gallery2, gallery3;
    File file = null;
    File file1 = null;
    File file2 = null;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_spare_seller_info, container, false);

        sharedPreferences = getActivity().getSharedPreferences(AppConstant.MOTORTRADER,
                Context.MODE_PRIVATE);

        userID = sharedPreferences.getString(
                AppConstant.USERID, "");
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
/**
 * check internet connection
 */
        checkInternet();


        edtname = (EditText) rootview.findViewById(R.id.edt_spare_name);
        edtemail = (EditText) rootview.findViewById(R.id.edt_spare_email);
        edtnumber = (EditText) rootview.findViewById(R.id.edt_spare_number);
        edtAddress = (EditText) rootview.findViewById(R.id.edt_spare_add);

        btnSubmit = (Button) rootview.findViewById(R.id.btn_Spare_Submit);


        if (getArguments() != null) {
            Bundle bundle = this.getArguments();
            makeid = bundle.getString("MAKE_ID");
            modelId = bundle.getString("MODEL_ID");
            strLocation = bundle.getString("LOCATION");
            strYear = bundle.getString("SELECTED_YEAR");
            strPrice = bundle.getString("PRICE");
            strDes = bundle.getString("DESCRIPTION");
            strSpare = bundle.getString("SPARE_PARTS");
            gallery1 = bundle.getString("IMAGE_GALLERY_1");
            gallery2 = bundle.getString("IMAGE_GALLERY_2");
            gallery3 = bundle.getString("IMAGE_GALLERY_3");


        }

        Log.e("strLocation",""+strLocation);

        if (gallery1 != null) {
            file = new File(gallery1);
        }
        if (gallery2 != null){
            file1 = new File(gallery2);
        }
        if (gallery3!=null){
            file2 = new File(gallery3);

        }




        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strname = edtname.getText().toString();
                stremail = edtemail.getText().toString();
                strnumber = edtnumber.getText().toString();
                straddress = edtAddress.getText().toString();

                boolean receive = validation();
                if (receive) {

                    new ExecuteSparePost().execute();

                }

            }
        });

        return rootview;
    }

    private boolean validation() {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";
        if (edtname.getText().toString().equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your  name", "OK");
            return false;
        }


        if (edtemail.getText().toString().matches("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter valid email address", "OK");
            return false;
        }

        if (!edtemail.getText().toString().matches(emailPattern)) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter valid email address", "OK");
            return false;
        }

        if (edtnumber.getText().toString().equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your phone number", "OK");
            return false;
        }


        if (edtAddress.getText().toString().equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your city", "OK");
            return false;
        }


        return true;
    }


    class ExecuteSparePost extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            return uploadFile();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            UI.hideProgressDialog();


            if (s.contains("true")) {
              /*  LogMessage.showDialog(getActivity(), "", "Spare part added successfully", "OK");*/
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("").setMessage("Spare part added successfully").setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                                getFragmentManager().popBackStack(
                                        SparePictures.class.getName(),
                                        FragmentManager.POP_BACK_STACK_INCLUSIVE);

                            }
                        });

                AlertDialog dialog = alertDialog.create();
                dialog.show();
                TextView messageText = (TextView) dialog
                        .findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER);

            }


        }
    }

    private String uploadFile() {

        String responseString = null;

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(AppConstant.MOTO
                + AppConstant.SPARE_POST);
        PostAPi entity = new PostAPi(getActivity());



        try {

            Log.e("file",""+file);
            Log.e("file1",""+file1);
            Log.e("file2",""+file2);

            entity.addPart("userId", new StringBody(userID));
            entity.addPart("make", new StringBody(makeid));
            entity.addPart("model", new StringBody(modelId));
            entity.addPart("sparename", new StringBody(strSpare));
            entity.addPart("year", new StringBody(strYear));
            entity.addPart("state", new StringBody(strLocation));
            entity.addPart("price", new StringBody(strPrice));
            entity.addPart("desc", new StringBody(strDes));
            entity.addPart("address", new StringBody(straddress));
            entity.addPart("sellername", new StringBody(strname));
            entity.addPart("email", new StringBody(stremail));
            entity.addPart("phone", new StringBody(strnumber));
            entity.addPart("image1", new FileBody(file));
            entity.addPart("image2", new FileBody(file1));
            entity.addPart("image3", new FileBody(file2));

        } catch (Exception e) {

        }

        httppost.setEntity(entity);
        try {
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                Log.e("statusCode", "" + statusCode);

                responseString = EntityUtils.toString(r_entity);
            } else {
                Log.e("else", "" + statusCode);
                responseString = "Error occurred! Http Status Code: "
                        + statusCode;
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            responseString = e.toString();
        } catch (IOException e) {
            Log.e("Io", "" + e.toString());
            responseString = e.toString();
        }


        return responseString;
    }
    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }

}
