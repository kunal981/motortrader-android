package com.poliveira.apps.materialtests.PaymentGateway;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.Util.AppConstant;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONObject;

/**
 * Created by ${ShalviSharma} on 10/23/15.
 */
public class PaymentActivity_ extends FragmentActivity {
    TextView headerTitle;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String user_id;
    JSONObject jsonObject;
    Button btnPay;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_payment);

        btnPay=(Button)findViewById(R.id.btnPayNow);
        sharedPreferences = getSharedPreferences(AppConstant.MOTORTRADER,
                Context.MODE_PRIVATE);

        user_id = sharedPreferences.getString(AppConstant.USERID, "");


       new ExecutePaymentResponse().execute(user_id);

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentPayment = new Intent();
                intentPayment.setAction(Intent.ACTION_VIEW);
                intentPayment.addCategory(Intent.CATEGORY_BROWSABLE);
                intentPayment.setData(Uri
                        .parse("https://www.motortrader.ng/car_post_payment.php"));
                startActivity(intentPayment);


            }
        });



    }


    class ExecutePaymentResponse extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String response = WSConnector.postPaymentResponse(params[0]);

            Log.e("response", "" + response);

            return response;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);



            if (result.contains("true")) {

                Toast.makeText(PaymentActivity_.this,"True",Toast.LENGTH_SHORT).show();
                finish();


            }
            else {
                Toast.makeText(PaymentActivity_.this,"false",Toast.LENGTH_SHORT).show();
               // new ExecutePaymentResponse().execute(user_id);
            }
        }
    }






    @Override
    protected void onRestart() {
        super.onRestart();
        new ExecutePaymentResponse().execute(user_id);
    }
}

