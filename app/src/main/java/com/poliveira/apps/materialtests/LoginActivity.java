package com.poliveira.apps.materialtests;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.AppConstant;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONObject;

/**
 * Created by ${ShalviSharma} on 7/29/15.
 */
public class LoginActivity extends Activity {

    Button btnLogin,btnRegister;
    TextView  txtForgot;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    String email, pwd;
    EditText edtUsername, edtPwd;

    JSONObject jsonObject;
    String message, uid;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        cd = new ConnectionDetector(LoginActivity.this);
        isInternetPresent = cd.isConnectingToInternet();
        sharedPreferences = getSharedPreferences(AppConstant.MOTORTRADER,
                Context.MODE_PRIVATE);
        /**
         * check internet connection
         */
        checkInternet();
        btnLogin = (Button) findViewById(R.id.button_login);
        btnRegister = (Button) findViewById(R.id.btn_register);
        txtForgot = (TextView) findViewById(R.id.txt_forgotPwd);
        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtPwd = (EditText) findViewById(R.id.edt_password);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email = edtUsername.getText().toString();
                pwd = edtPwd.getText().toString();

                boolean receive = validation(email, pwd);
                if (receive && isInternetPresent) {
                    new ExecuteLogin().execute(email, pwd);

                }


            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {
                Intent i_register = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i_register);
                overridePendingTransition(R.anim.slide_in_right,
                        R.anim.slide_out_left);


            }
        });


        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i_forgot = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(i_forgot);
                overridePendingTransition(R.anim.slide_in_right,
                        R.anim.slide_out_left);


            }
        });
    }


    private boolean validation(String email, String pwd) {
        // TODO Auto-generated method stub
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";

        if (email.matches("")) {
            LogMessage.showDialog(LoginActivity.this, null,
                    "Please enter your email address", "OK");
            return false;
        }

        if (!email.matches(emailPattern)) {
            LogMessage.showDialog(LoginActivity.this, null,
                    "Please enter valid email address", "OK");
            return false;
        }

        if (pwd.matches("")) {
            LogMessage.showDialog(LoginActivity.this, null,
                    "Please enter your password", "OK");
            return false;
        }

        return true;
    }


    class ExecuteLogin extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String result = WSConnector.login(params[0], params[1]);
            return result;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();

          UI.showProgressDialog(LoginActivity.this);

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
        UI.hideProgressDialog();

            if (result.contains("true")) {

                updateUI(result);


            } else {


                LogMessage.showDialog(LoginActivity.this, "",
                        "Invalid login details", "OK");
            }


        }
    }


    public void updateUI(String result) {

        try {

            jsonObject = new JSONObject(result);

            uid = jsonObject.getString("userId");
            message = jsonObject.getString("message");



            if (uid != null) {
                sharedPrefernces();
            }

            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.slide_in_right,
                    R.anim.slide_out_left);
            finish();

        } catch (Exception e) {
            e.getMessage();
        }

    }


    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
            alertDialog.setTitle("").setMessage("Network connection not found").setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            finish();
                        }
                    });

            AlertDialog dialog = alertDialog.create();
            dialog.show();
            TextView messageText = (TextView) dialog
                    .findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER);

        }
    }


    public void sharedPrefernces() {
        editor = sharedPreferences.edit();
        editor.putString(AppConstant.USERID, String.valueOf(uid));
        editor.putString(AppConstant.USERNAME, email);
        editor.putString(AppConstant.PASSWORD, pwd);


        editor.commit();

    }

}
