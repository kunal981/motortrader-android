package com.poliveira.apps.materialtests.Modal;

/**
 * Created by ${ShalviSharma} on 8/13/15.
 */
public class AdvanceCarModal {

    private String id;
    private String name;
    private String model;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
