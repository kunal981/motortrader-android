package com.poliveira.apps.materialtests.AddyourCarFragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.poliveira.apps.materialtests.PaymentGateway.PaymentActivity;
import com.poliveira.apps.materialtests.PostAPi;
import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.AppConstant;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

/**
 * Created by ${ShalviSharma} on 7/30/15.
 */
public class SellerInfoFragment extends Fragment {
    EditText edtname, edtemail, edtnumber, edtAddress;
    Button btnSubmit;
    String strname, stremail, strnumber, straddress, userID, strLocation, user_id;
    SharedPreferences sharedPreferences;
    String makeid, modelId, yearSearch, strPrice, strDes, bodytypel, transType, fuelType, gallery1, gallery2, gallery3, camera1, camera2, camera3;
    File file = null;
    File file1 = null;
    File file2 = null;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    public static int RESULT_OK = 1;
    String resp = "false";


    public static SellerInfoFragment newInstance() {
        SellerInfoFragment sellerInfoFragment = new SellerInfoFragment();
        return sellerInfoFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        sharedPreferences = getActivity().getSharedPreferences(AppConstant.MOTORTRADER,
                Context.MODE_PRIVATE);

        user_id = sharedPreferences.getString(AppConstant.USERID, "");

        Log.e("user_id", "" + user_id);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_seller_info, container, false);
        sharedPreferences = getActivity().getSharedPreferences(AppConstant.MOTORTRADER,
                Context.MODE_PRIVATE);

        userID = sharedPreferences.getString(
                AppConstant.USERID, "");
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
/**
 *
 * check internet connection
 */
        checkInternet();

        FragmentManager fragmentManager = getFragmentManager();
        if (getArguments() != null) {
            Bundle bundle = this.getArguments();
            makeid = bundle.getString("MAKE_ID");
            modelId = bundle.getString("MODEL_ID");
            yearSearch = bundle.getString("SELECTED_YEAR");
            bodytypel = bundle.getString("BODY_TYPE");
            fuelType = bundle.getString("FUEL_TYPE");
            transType = bundle.getString("TRANS_TYPE");
            strPrice = bundle.getString("PRICE");
            strLocation = bundle.getString("LOCATION");
            strDes = bundle.getString("DESCRIPTION");
            gallery1 = bundle.getString("IMAGE_GALLERY_1");
            gallery2 = bundle.getString("IMAGE_GALLERY_2");
            gallery3 = bundle.getString("IMAGE_GALLERY_3");


        }


        edtname = (EditText) rootview.findViewById(R.id.edt_name_add);
        edtemail = (EditText) rootview.findViewById(R.id.edt_email_add);
        edtnumber = (EditText) rootview.findViewById(R.id.edt_number_add);
        edtAddress = (EditText) rootview.findViewById(R.id.edt_address_add);

        btnSubmit = (Button) rootview.findViewById(R.id.btnSubmitAdd);


        if (gallery1 != null) {
            file = new File(gallery1);
        }
        if (gallery2 != null) {
            file1 = new File(gallery2);
        }
        if (gallery3 != null) {
            file2 = new File(gallery3);

        }


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strname = edtname.getText().toString();
                stremail = edtemail.getText().toString();
                strnumber = edtnumber.getText().toString();
                straddress = edtAddress.getText().toString();

                /*FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                AddCarDetailsFragment fragment = new AddCarDetailsFragment();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();*/

                Log.e("resp", "" + resp);



                    if (resp.equals("false")){
                        new ExecutePaymentResponse().execute(user_id);
                    }







            }
        });
        return rootview;
    }


    private boolean validation( String strname, String stremail, String strnumber, String straddress) {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";
        if (strname.equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your  name", "OK");
            return false;
        }


        if (stremail.matches("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your email address", "OK");
            return false;
        }

        if (!edtemail.getText().toString().matches(emailPattern)) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter valid email address", "OK");
            return false;
        }

        if (strnumber.equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your phone number", "OK");
            return false;
        }


        if (straddress.equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your city", "OK");
            return false;
        }



        return true;
    }


    class ExecutePaymentResponse extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String response = WSConnector.postPaymentResponse(params[0]);

            Log.e("response", "" + response);

            return response;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            UI.hideProgressDialog();

            if (result.contains("false")) {


                resultPayment(result);


            } else {
                boolean receive = validation(strname, stremail, strnumber, straddress);
                if (receive) {
                    new ExecuteCarPost().execute();
                }
            }
        }
    }

    private void resultPayment(String result) {

        try {
            JSONObject jsonObject = new JSONObject(result);

            resp = jsonObject.getString("success");


            if (resp.contains("false")) {
                String message = jsonObject.getString("message");

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("ALERT").setMessage("You will need to pay fees for posting car.").setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // TODO Auto-generated method stub

                                        Intent intent_payment = new Intent(getActivity(), PaymentActivity.class);
                                        intent_payment.putExtra("emailAddress", stremail);
                                        intent_payment.putExtra("number", strnumber);
                                        intent_payment.putExtra("customerId", user_id);
                                        startActivity(intent_payment);

                                    }
                                }


                        ).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


                AlertDialog dialog = alertDialog.create();
                dialog.show();
                TextView messageText = (TextView) dialog
                        .findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER);


            }


        } catch (Exception e) {

        }
    }


    class ExecuteCarPost extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            return uploadFile();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            UI.hideProgressDialog();


            if (s.contains("true")) {
                /*LogMessage.showDialog(getActivity(),"","Car added successfully","OK");*/
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("").setMessage("Car added successfully").setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                                getFragmentManager().popBackStack(
                                        CarPictureFragment.class.getName(),
                                        FragmentManager.POP_BACK_STACK_INCLUSIVE);

                            }
                        });

                AlertDialog dialog = alertDialog.create();
                dialog.show();
                TextView messageText = (TextView) dialog
                        .findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER);

            }


        }
    }

    private String uploadFile() {

        String responseString = null;

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(AppConstant.MOTO
                + AppConstant.CAR_POST);
        PostAPi entity = new PostAPi(getActivity());

        try {


            entity.addPart("userId", new StringBody(userID));
            entity.addPart("make", new StringBody(makeid));
            entity.addPart("model", new StringBody(modelId));
            entity.addPart("year", new StringBody(yearSearch));
            entity.addPart("transmession", new StringBody(transType));
            entity.addPart("fuel", new StringBody(fuelType));
            entity.addPart("body", new StringBody(bodytypel));
            entity.addPart("price", new StringBody(strPrice));
            entity.addPart("desc", new StringBody(strDes));
            entity.addPart("address", new StringBody(straddress));
            entity.addPart("state", new StringBody(strLocation));
            entity.addPart("sellername", new StringBody(strname));
            entity.addPart("email", new StringBody(stremail));
            entity.addPart("phone", new StringBody(strnumber));
            entity.addPart("image1", new FileBody(file));
            entity.addPart("image2", new FileBody(file1));
            entity.addPart("image3", new FileBody(file2));

        } catch (Exception e) {

        }

        httppost.setEntity(entity);
        try {
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                Log.e("statusCode", "" + statusCode);

                responseString = EntityUtils.toString(r_entity);
            } else {
                Log.e("else", "" + statusCode);
                responseString = "Error occurred! Http Status Code: "
                        + statusCode;
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            responseString = e.toString();
        } catch (IOException e) {
            Log.e("Io", "" + e.toString());
            responseString = e.toString();
        }


        return responseString;
    }


    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }

}
