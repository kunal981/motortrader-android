package com.poliveira.apps.materialtests.AddyourCarFragment;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ${ShalviSharma} on 7/30/15.
 */
public class AddCarDetailsFragment extends Fragment {

    Spinner spinnermake;
    Spinner spinnermodel;
    Spinner spinneryear;
    Spinner spinnertrans;
    Spinner spinnerfuel;
    Spinner spinnerbody;
    Spinner spinnerloc;
    Spinner spinnertype;
    String idLoc, user_id;
    String[] spinnerType = {"Select Type", "New", "Quality Used"};
    String[] spinnerTrans = {"Any Transmission", "Manual", "Automatic", "Other"};
    String[] spinnerFuel = {"Any Fuel type", "Petrol", "Diesel", "Hybrid", "Other"};
    String idMake, idModel, idBody, selectedYear, strPrice, strDesc, selectedType;
    ArrayAdapter<String> adapterMake, adapterMakeModel, adapterYear, adapterTrans, adapterFuel, adapterBody, adapterLoc, adapterType;
    Button btnNext;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    JSONObject jsonObject;
    ArrayList<String> arrayListName, arrayListIDMake, arrayListModel, arrayListBodyName, arrayListIDModel, arrayListIDBody, arrayListLocation,
            arrayListIDLocation;
    int Fuelpos, transpos;
    EditText edtPrice, edtDescription;
    TextView headerTitle;
    String back = "0";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static AddCarDetailsFragment newInstance() {
        AddCarDetailsFragment addYourCarFragment = new AddCarDetailsFragment();
        return addYourCarFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_add_your_car, container, false);
        btnNext = (Button) rootview.findViewById(R.id.b_next_add);

        spinnermake = (Spinner) rootview.findViewById(R.id.spinner_make_add);
        spinnermodel = (Spinner) rootview.findViewById(R.id.spinner_model_add);
        spinneryear = (Spinner) rootview.findViewById(R.id.spinner_year_add);
        spinnertrans = (Spinner) rootview.findViewById(R.id.spinner_transmission_add);
        spinnerfuel = (Spinner) rootview.findViewById(R.id.spinner_fuel_add);
        spinnerbody = (Spinner) rootview.findViewById(R.id.spinner_body_add);
        spinnerloc = (Spinner) rootview.findViewById(R.id.spinner_add_state);
       /* spinnertype = (Spinner) rootview.findViewById(R.id.spinner_make_type);*/

        edtPrice = (EditText) rootview.findViewById(R.id.edittext_price_add);
        edtDescription = (EditText) rootview.findViewById(R.id.edt_desc_add);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");
        headerTitle = (TextView) activity.findViewById(R.id.mytext);

        headerTitle.setText("Add your Car");
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();


/**
 * check internet connection
 */
        checkInternet();

        arrayListName = new ArrayList<>();
        arrayListIDMake = new ArrayList<>();
        arrayListModel = new ArrayList<>();
        arrayListBodyName = new ArrayList<>();

        arrayListIDModel = new ArrayList<>();

        arrayListIDBody = new ArrayList<>();

        arrayListLocation = new ArrayList<>();
        arrayListIDLocation = new ArrayList<>();
        String[] year = getResources().getStringArray(R.array.spinnerYear);


 /*
        Make API
         */


        new ExecuteMake().execute();


        /*
        Body List API
         */


        new ExecuteBodyList().execute();
   /*
        Car Location List API
         */


        new ExecuteLocationList().execute();


        adapterYear = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, year);
        adapterYear.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
        spinneryear.setAdapter(adapterYear);
        spinneryear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedYear = parent.getItemAtPosition(position).toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                LogMessage.showDialog(getActivity(), "", "Please select Year", "OK");
            }
        });


        adapterTrans = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, spinnerTrans);
        adapterTrans.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
        spinnertrans.setAdapter(adapterTrans);
        spinnertrans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                transpos = position;


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                LogMessage.showDialog(getActivity(), "", "Please select transmission type", "OK");
            }
        });
        adapterFuel = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, spinnerFuel);
        adapterFuel.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
        spinnerfuel.setAdapter(adapterFuel);

        spinnerfuel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Fuelpos = position;


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                LogMessage.showDialog(getActivity(), "", "Please select Fuel type", "OK");
            }
        });


      /*  adapterType = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, spinnerType);
        adapterType.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
        spinnertype.setAdapter(adapterType);
        spinnertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedType = parent.getItemAtPosition(position).toString();
                Log.e("selectedType",""+selectedType);



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                LogMessage.showDialog(getActivity(), "", "Please select Year", "OK");
            }
        });*/
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strPrice = edtPrice.getText().toString();
                strDesc = edtDescription.getText().toString();


                boolean receive = validation(idMake, idModel, selectedYear, idLoc, "" + Fuelpos, "" + transpos, idBody, strPrice, strDesc);
                if (receive) {

                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    CarPictureFragment fragment = new CarPictureFragment();

                    Bundle savedInstanceState = new Bundle();
                    savedInstanceState.putString("MAKE_ID", idMake);
                    savedInstanceState.putString("MODEL_ID", idModel);
                    savedInstanceState.putString("SELECTED_YEAR", selectedYear);
                    savedInstanceState.putString("LOCATION", idLoc);
                    savedInstanceState.putString("FUEL_TYPE", "" + Fuelpos);
                    savedInstanceState.putString("TRANS_TYPE", "" + transpos);
                    savedInstanceState.putString("BODY_TYPE", idBody);
                    savedInstanceState.putString("PRICE", strPrice);
                    savedInstanceState.putString("DESCRIPTION", strDesc);

                    fragmentTransaction.replace(R.id.container, fragment).addToBackStack(fragment.getClass().getName());
                    fragment.setArguments(savedInstanceState);
                    fragmentTransaction.commit();
                }
            }
        });


        rootview.setFocusableInTouchMode(true);
        rootview.requestFocus();
        rootview.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub


                if (keyCode == KeyEvent.KEYCODE_BACK && back.equals("0")) {
                    back = "1";

                    Toast.makeText(getActivity(), "Press again to exit!", Toast.LENGTH_SHORT).show();
                    return true;

                } else {
                    return false;
                }

            }
        });


        return rootview;
    }


    private boolean validation(String idMake, String idModel, String selectedYear, String idLoc, String Fuelpos, String transpos, String idBody, String strPrice, String strDesc) {
        // TODO Auto-generated method stub
        if (idMake.matches("0")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please select make", "OK");
            return false;
        }
        if (idModel.matches("0")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please select model", "OK");
            return false;
        }
        if (selectedYear.matches("Any Year")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please select year", "OK");
            return false;
        }
        if (idLoc.matches("0")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please select state", "OK");
            return false;
        }


        if (transpos.matches("0")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please select transmission ", "OK");
            return false;
        }

        if (Fuelpos.matches("0")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please select Fuel type", "OK");
            return false;
        }

        if (idBody.matches("0")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please select Body type", "OK");
            return false;
        }


        if (strPrice.matches("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter price", "OK");
            return false;
        }


        if (strDesc.matches("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter description", "OK");
            return false;
        }


        return true;
    }


    /*
    Car Make Execution
     */


    class ExecuteMake extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String url = WSConnector.getMake();
            Log.e("url--", "" + url);

            return url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            UI.hideProgressDialog();
            if (result.contains("true")) {


                updateUI(result);
                if (arrayListName != null) {

                    adapterMake = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, arrayListName);
                    adapterMake.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
                    spinnermake.setAdapter(adapterMake);


                    spinnermake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                            idMake = arrayListIDMake.get(position);
                            try {
                                new ExecuteModel().execute(idMake);
                            } catch (Exception e) {

                            }


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            LogMessage.showDialog(getActivity(), "", "Please select Make", "OK");
                        }
                    });
                }
            }


        }
    }


    public void updateUI(String result) {

        try {
            arrayListName.add("Any Makes");
            arrayListIDMake.add("0");

            jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i <= jsonArray.length(); i++) {

                JSONObject jsonInnerObject = jsonArray.getJSONObject(i);

                arrayListIDMake.add(jsonInnerObject.getString("id"));


                arrayListName.add(jsonArray.getJSONObject(i).getString("name"));

            }


        } catch (Exception e) {

        }

    }


      /*
    Car Model Execution
     */


    class ExecuteModel extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String url = WSConnector.postCarModel(params[0]);

            return url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();*/


        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
           /* pDialog.dismiss();*/
            if (result.contains("true")) {
                arrayListIDModel.clear();
                arrayListModel.clear();
                updateModel(result);


                adapterMakeModel = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, arrayListModel);
                adapterMakeModel.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
                spinnermodel.setAdapter(adapterMakeModel);


                spinnermodel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                        idModel = arrayListIDModel.get(position);


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        LogMessage.showDialog(getActivity(), "", "Please select Model", "OK");
                    }
                });
            }
        }
    }

    public void updateModel(String result) {

        try {
            arrayListIDModel.add("0");
            arrayListModel.add("Any Model");


            jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i <= jsonArray.length(); i++) {

                arrayListIDModel.add(jsonArray.getJSONObject(i).getString("id"));

                arrayListModel.add(jsonArray.getJSONObject(i).getString("model"));

            }


        } catch (Exception e) {

        }

    }


    /*
        Body list execution
     */

    class ExecuteBodyList extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            String url = WSConnector.getBodyList();

            return url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.contains("true")) {

                updateBody(result);


                adapterBody = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, arrayListBodyName);
                adapterBody.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
                spinnerbody.setAdapter(adapterBody);
                spinnerbody.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        idBody = arrayListIDBody.get(position);


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        LogMessage.showDialog(getActivity(), "", "Please select Body type", "OK");


                    }
                });

            }
        }
    }

    public void updateBody(String result) {

        try {
            arrayListIDBody.add("0");
            arrayListBodyName.add("Any Body Type");

            jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("data");


            for (int i = 0; i <= jsonArray.length(); i++) {


                arrayListIDBody.add(jsonArray.getJSONObject(i).getString("id"));

                arrayListBodyName.add(jsonArray.getJSONObject(i).getString("name"));

            }


        } catch (Exception e) {

        }

    }


    /*
        Location list execution
     */

    class ExecuteLocationList extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            String response = WSConnector.getLocationList();

            return response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.contains("true")) {
                updateLocation(s);
                adapterLoc = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item_, arrayListLocation);
                adapterLoc.setDropDownViewResource(R.layout.custom_spinner_item_dropdown_);
                spinnerloc.setAdapter(adapterLoc);
                spinnerloc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        idLoc = arrayListIDLocation.get(position);


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }
    }

    public void updateLocation(String s) {
        try {
            arrayListIDLocation.add("0");
            arrayListLocation.add("Any State");

            jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("data");


            for (int i = 0; i <= jsonArray.length(); i++) {

                arrayListIDLocation.add(jsonArray.getJSONObject(i).getString("id"));

                arrayListLocation.add(jsonArray.getJSONObject(i).getString("name"));

            }


        } catch (Exception e) {

        }

    }


    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            LogMessage.showDialog(getActivity(), "", "Network connection not found", "OK");


        }
    }


   /* @Override
    public void onResume() {
        super.onResume();
        new ExecutePaymentResponse().execute(user_id);
    }*/
}
