package com.poliveira.apps.materialtests.WebServices;

import com.poliveira.apps.materialtests.Util.AppConstant;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shalvi Sharma
 */
public class WSConnector {

	/*

	Login API

	*/

    public static String login(String email, String password) {

        String url = AppConstant.MOTO + AppConstant.LOGIN;
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("email", email));
        pairs.add(new BasicNameValuePair("password", password));

        String result = WSAdapter.postJSONObject(url, pairs);


        return result;

    }

	/*

	Forgot pwd API

	*/


    public static String forgotpwd(String email) {

        String url = AppConstant.MOTO + AppConstant.FORGOTPASSWORD;
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("userEmail", email));


        String result = WSAdapter.postJSONObject(url, pairs);


        return result;

    }


	/*

	Register API

	*/


    public static String Register(String firstname, String lastname,
                                  String userEmail, String userPassword, String userlocation) {
        String URL = AppConstant.MOTO + AppConstant.REGISTER;
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("firstname", firstname));
        pairs.add(new BasicNameValuePair("lastname", lastname));
        pairs.add(new BasicNameValuePair("userEmail", userEmail));
        pairs.add(new BasicNameValuePair("userPassword", userPassword));
        pairs.add(new BasicNameValuePair("userlocation", userlocation));

        String result = WSAdapter.postJSONObject(URL, pairs);


        return result;

    }


	/*

	All Car List API

	*/


    public static String getCarList() {
        String URL = AppConstant.MOTO + AppConstant.CARLIST;
        String result = WSAdapter.getJSONObject(URL);
        return result;

    }


	/*

	Car Details API

	*/


    public static String post_carDetails(String car_id) {
        String URL = AppConstant.MOTO + AppConstant.CARDETAILS;
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("car_id", car_id));


        String result = WSAdapter.postJSONObject(URL, pairs);

        return result;

    }



	/*

	Reset Pwd API

	*/


    public static String postResetPwd(String uid, String currentpsw, String newpass) {
        String URL = AppConstant.MOTO + AppConstant.RESETPASSWORD;

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("uid", uid));
        pairs.add(new BasicNameValuePair("currentpsw", currentpsw));
        pairs.add(new BasicNameValuePair("newpass", newpass));

        String result = WSAdapter.postJSONObject(URL, pairs);

        return result;

    }

/*

	Write to us  API

	*/


    public static String postWritetoUs(String carID, String txtName, String txtEmail, String phone, String txtMessage) {
        String URL = AppConstant.MOTO + AppConstant.WRITETOUS;

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("carID", carID));
        pairs.add(new BasicNameValuePair("txtName", txtName));
        pairs.add(new BasicNameValuePair("txtEmail", txtEmail));
        pairs.add(new BasicNameValuePair("phone", phone));
        pairs.add(new BasicNameValuePair("txtMessage", txtMessage));

        String result = WSAdapter.postJSONObject(URL, pairs);

        return result;

    }



	/*

	Spare Part  API

	*/


    public static String getSparePart() {
        String URL = AppConstant.MOTO + AppConstant.SPAREPARTS;
        String result = WSAdapter.getJSONObject(URL);

        return result;

    }

    /*
        Make API
     */
    public static String getMake() {
        String URL = AppConstant.MOTO + AppConstant.MAKELISTING;
        String result = WSAdapter.getJSONObject(URL);
        return result;

    }



	/*

	Car Model  API

	*/


    public static String postCarModel(String makeId) {
        String URL = AppConstant.MOTO + AppConstant.CARMODEL;

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("makeId", makeId));

        String result = WSAdapter.postJSONObject(URL, pairs);

        return result;

    }


    /*
        Body List API
     */
    public static String getBodyList() {
        String URL = AppConstant.MOTO + AppConstant.BODYLIST;
        String result = WSAdapter.getJSONObject(URL);
        return result;

    }


    /*
        Location List API
     */
    public static String getLocationList() {
        String URL = AppConstant.MOTO + AppConstant.CARLOCATION;
        String result = WSAdapter.getJSONObject(URL);
        return result;

    }



	/*

	Advance search API

	*/


    public static String postAdvanceSearch(String make, String model,
                                           String year, String location, String fuelType, String transmison, String bodyType) {
        String URL = AppConstant.MOTO + AppConstant.ADVANCE_SEARCH;
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("make", make));
        pairs.add(new BasicNameValuePair("model", model));
        pairs.add(new BasicNameValuePair("year", year));
        pairs.add(new BasicNameValuePair("location", location));
        pairs.add(new BasicNameValuePair("fuelType", fuelType));
        pairs.add(new BasicNameValuePair("transmison", transmison));
        pairs.add(new BasicNameValuePair("bodyType", bodyType));


        String result = WSAdapter.postJSONObject(URL, pairs);


        return result;

    }


	/*

	Spare Details API

	*/


    public static String post_spareDetails(String spareid) {
        String URL = AppConstant.MOTO + AppConstant.SPAREDETAILS;
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("spareid", spareid));


        String result = WSAdapter.postJSONObject(URL, pairs);

        return result;

    }


	/*

	Spare Write to us  API

	*/


    public static String postWritetoUsSpare(String spareid, String txtName, String txtEmail, String phone, String txtMessage) {
        String URL = AppConstant.MOTO + AppConstant.SPARE_WRITE;

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("spareid", spareid));
        pairs.add(new BasicNameValuePair("txtName", txtName));
        pairs.add(new BasicNameValuePair("txtEmail", txtEmail));
        pairs.add(new BasicNameValuePair("phone", phone));
        pairs.add(new BasicNameValuePair("txtMessage", txtMessage));

        String result = WSAdapter.postJSONObject(URL, pairs);

        return result;

    }



	/*

	Payment Response  API

	*/


    public static String postPaymentResponse(String user_id) {
        String URL = AppConstant.MOTO + AppConstant.CHECK_PAYMENT;

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("user_id", user_id));


        String result = WSAdapter.postJSONObject(URL, pairs);

        return result;

    }


}
