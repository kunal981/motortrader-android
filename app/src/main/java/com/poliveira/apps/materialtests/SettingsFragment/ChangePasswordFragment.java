package com.poliveira.apps.materialtests.SettingsFragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.poliveira.apps.materialtests.R;
import com.poliveira.apps.materialtests.UserInteface.UI;
import com.poliveira.apps.materialtests.Util.AppConstant;
import com.poliveira.apps.materialtests.Util.ConnectionDetector;
import com.poliveira.apps.materialtests.Util.LogMessage;
import com.poliveira.apps.materialtests.WebServices.WSConnector;

import org.json.JSONObject;

/**
 * Created by ${ShalviSharma} on 7/31/15.
 */
public class ChangePasswordFragment extends Fragment {
    TextView headerTitle;
    SharedPreferences sharedPreferences;
    EditText edtCurrent,edtNewpwd,edtRepwd;
    String uid;
    Button btnSubmitPwd;
    String strCurrent,strNewpwd,strRePwd,storedPwd;
    ConnectionDetector cd;
    boolean isInternetPresent = false;

    JSONObject jsonObject;
    SharedPreferences.Editor editor;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_change_password,container,false);

        AppCompatActivity activity = (AppCompatActivity) getActivity();

        activity.getSupportActionBar().setTitle("");

        cd = new ConnectionDetector(getActivity());
        sharedPreferences = getActivity().getSharedPreferences(AppConstant.MOTORTRADER,
                Context.MODE_PRIVATE);
        uid = sharedPreferences.getString(AppConstant.USERID, "");
        storedPwd = sharedPreferences.getString(AppConstant.PASSWORD, "");
        isInternetPresent = cd.isConnectingToInternet();
        checkInternet();
        Log.e("storedPwd--", "" + storedPwd);

        headerTitle = (TextView) activity.findViewById(R.id.mytext);
        edtCurrent = (EditText)rootview.findViewById(R.id.edt_current_pwd);
        edtNewpwd = (EditText)rootview.findViewById(R.id.edt_new_pwd);
        edtRepwd = (EditText)rootview.findViewById(R.id.edt_re_pwd);
        btnSubmitPwd = (Button)rootview.findViewById(R.id.button_submit_reset);

        headerTitle.setText("Reset Password");

        btnSubmitPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strCurrent = edtCurrent.getText().toString();
                strNewpwd = edtNewpwd.getText().toString();
                strRePwd = edtRepwd.getText().toString();

                boolean receive = validatation_method();
                if (receive && isInternetPresent) {

                    Log.e("strCurrent", "" + strCurrent);
                    Log.e("strNewpwd", "" + strNewpwd);
                    Log.e("strRePwd", "" + strRePwd);
                    new ExecuteResetPwd().execute(uid, strCurrent, strNewpwd);
                }


            }
        });

        return rootview;
    }



    private boolean validatation_method() {
        // TODO Auto-generated method stub
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";
        if (edtCurrent.getText().toString().equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your current password", "OK");
            return false;
        }
        if (edtNewpwd.getText().toString().equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please enter your new password ", "OK");
            return false;
        }

        if (edtRepwd.getText().toString().equals("")) {
            LogMessage.showDialog(getActivity(), null,
                    "Please again enter your new password ", "OK");


            return false;
        }

        if (!edtRepwd.getText().toString().trim()
                .matches(edtNewpwd.getText().toString())) {
            LogMessage.showDialog(getActivity(), null,
                    "Password does not match the re-password",
                    "OK");
            return false;
        }

        if (!strCurrent.matches(storedPwd)){
            LogMessage.showDialog(getActivity(), null,
                    "Please enter correct current password",
                    "OK");
            return false;
        }


        return true;
    }




    class ExecuteResetPwd extends AsyncTask<String,Integer,String>{

        @Override
        protected String doInBackground(String... params) {

            String URL = WSConnector.postResetPwd(params[0],params[1],params[2]);

            Log.e("URL",""+URL);
            return URL;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UI.showProgressDialog(getActivity());
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            UI.hideProgressDialog();
            if (result.contains("true")) {
                updateUI(result);

            }
        }
    }

    public void updateUI(String result) {

        try {

            jsonObject = new JSONObject(result);

            String message = jsonObject.getString("message");
            sharedPrefernces();
            if (message!=null){
                edtCurrent.setText("");
                edtNewpwd.setText("");
                edtRepwd.setText("");
                LogMessage.showDialog(getActivity(), "", message, "OK");
            }





        }catch (Exception e){

        }

    }


    public void checkInternet() {
        // TODO Auto-generated method stub
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            // showAlertDialog(HomepageActivity.this, "Internet Connection",
            // "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet

//           LogMessage.showDialog(getActivity(),"No Internet Connection","You don't have internet connection.","OK");
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("").setMessage("Network connection not found").setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();


                        }
                    });

            AlertDialog dialog = alertDialog.create();
            dialog.show();
            TextView messageText = (TextView) dialog
                    .findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER);

        }
    }


    public void sharedPrefernces() {
        editor = sharedPreferences.edit();
        editor.putString(AppConstant.PASSWORD, String.valueOf(strNewpwd));



        editor.commit();

    }
}
